\section{Real Numbers}

The real numbers form the unique (up to isomorphism) complete totally ordered
field.
This chapter shall construct this structure.
There are many equivalent ways of doing this, but to take mileage of all order
theory presented so far Dedekind cuts shall be used.
The idea is similar to the approach constructing integers -- first construct
the positive reals and then extend to all reals.
Lastly, it shall be shown that the reals are indeed unique up to isomorphism.

\subsection{Positive Real Numbers}

From the positive rationals the positive reals shall be constructed.
The aim of this section is an axiom system for the positive reals from which
all reals can be constructed.
In effect, this is the only step in the construction that relies on the rational
numbers.

\begin{definition}
  A positive real number \(\xi\) is a subset of \(\pos{\setQ}\) such that
  \begin{romanenum}
    \item \(\xi \ne \varnothing\),
    \item \(\xi \ne \pos{\setQ}\),
    \item \(\xi\) is downward closed,
    \item \(\xi\) does not have a greatest element.
  \end{romanenum}
  The set of positive real numbers is denoted by \(\pos{\setR}\).
\end{definition}

It is easy to deduce the order properties.
The relation \(\le\) on \(\pos{\setR}\) is defined such that
\[ \xi \le \eta \iffs \xi \subseteq \eta. \]

\begin{proposition}
  \(\pos{\setR}\) is totally ordered under \(\le\).
\end{proposition}
\begin{proof}
  That \(\le\) is transitive and antisymmetric follows from that \(\subseteq\)
  is a partial order on \(\powset{\pos{\setQ}}\).
  To show that \(\le\) is total let \(\xi,\eta \in \pos{\setR}\).
  If \(\xi \le \eta\) there is nothing to prove.
  Otherwise there is an element \(x \in \xi\) such that \(x \notin \eta\).
  Now it must hold that \(y < x\) for all \(y \in \eta\) or otherwise
  \(x \le y\) so that \(x \in \eta\) because \(\eta\) is downward closed.
  Hence,
  \[ y \in \eta \impliess y < x \impliess y \in \xi \]
  so \(\eta \subseteq \xi\), that is \(\eta \le \xi\).
\end{proof}

The next property is what distinguishes the reals from the rationals, namely
completeness.

\begin{proposition}
  Every non-empty subset of \(\pos{\setR}\) with an upper bound has a least
  upper bound in \(\pos{\setR}\).
\end{proposition}
\begin{proof}
  Let \(S \subseteq \pos{\setR}\) be non-empty with an upper bound, say
  \(\gamma\).
  Let
  \[ \eta = \bigcup_{\xi \in S} \xi. \]
  It shall be shown that \(\eta\) is a positive real number.
  (i) Because \(\eta\) is a non-empty union of non-empty sets it is non-empty.
  (ii) Since \(\xi \subseteq \gamma\) for all \(\xi \in S\) one has
  \[ \eta \subseteq \gamma \subset \pos{\setQ}. \]
  (iii) By the dual of proposition \ref{prop:union-intersection-upward-closed},
  \(\eta\) is downward closed.
  (iv) Lastly it shall be shown that \(\eta\) has no greatest element.
  Let \(x \in \eta\).
  Then \(x \in \xi\) for some \(\xi \in S\).
  Since \(\xi\) has no greatest element there is \(y \in \xi\) such that
  \(x < y\), but then \(y \in \eta\), so \(\eta\) has no greatest element.
  Hence, \(\eta\) is a positive real number for which \(\xi \subseteq \eta\) for
  all \(\xi \in S\).
  Thus, \(\eta\) is an upper bound.
  By the argument above, given any upper bound \(\gamma\) it holds that
  \(\eta \subseteq \gamma\).
  Therefore \(\eta\) is the least upper bound of \(S\).
\end{proof}

\begin{lemma}
  A downward closed subset of \(\pos{\setQ}\) is proper if and only if it has
  an upper bound.
\end{lemma}
\begin{proof}
  Let \(A\) be a downward closed subset of \(\pos{\setQ}\).
  First suppose \(A\) is a proper subset.
  Then there is an \(x \in \pos{\setQ}\) such that \(x \notin A\).
  Because \(A\) is downward closed there cannot exist \(y \in A\) such that
  \(x \le y\), since that would imply \(x \in A\).
  Hence, \(y < x\) for all \(y \in A\) and \(x\) is an upper bound of \(A\).
  Conversely, suppose \(A\) has an upper bound, say \(x\).
  Then \(x + 1 \notin A\), so \(A\) is a proper subset of \(\pos{\setQ}\).
\end{proof}

Define addition and multiplication in \(\pos{\setR}\) by
\[
  \xi + \eta = \{x + y \ssep x \in \xi, y \in \eta\}
  \qandq
  \xi\eta = \{xy \ssep x \in \xi, y \in \eta\}.
\]
It must be shown that addition and multiplication are actually operators on
\(\pos{\setR}\).
Since the proofs are very similar, let \(*\) denote one of \(+\) and \(\cdot\).
Suppose \(\xi,\eta \in \pos{\setR}\).

(i) Because both \(\xi\) and \(\eta\) are non-empty there is \(x \in \xi\) and
\(y \in \eta\).
Hence,
\[ x * y \in \xi * \eta \]
so \(\xi * \eta\) is non-empty.

(ii) To show that it is a proper subset of \(\pos{\setQ}\) it suffices by the
lemma to show that \(\xi * \eta\) has an upper bound.
Let \(z_\xi\) and \(z_\eta\) be upper bounds of \(\xi\) and \(\eta\)
respectively.
Hence,
\[ x * y \le z_\xi * z_\eta \]
for all \(x \in \xi\) and \(y \in \eta\) showing that \(\xi * \eta\) has an
upper bound.

(iii) To show that \(\xi * \eta\) is downward closed suppose \(x \le y\) where
\(y \in \xi * \eta\).
Then \(y = y_\xi * y_\eta\) where \(y_\xi \in \xi\) and \(y_\eta \in \eta\).
When \(*\) means addition the situation is divided into cases.
First consider the case when \(x > y_\xi\).
Then
\[ x - y_\xi > 0. \]
Furthermore
\[ x \le y_\xi + y_\eta \impliess x - y_\xi \le y_\eta \]
so that \(x - y_\xi \in \eta\).
Now
\[ x = y_\xi + (x - y_\xi) \in \xi + \eta. \]
The case when \(x > y_\eta\) is similar.
Otherwise \(x \le y_\xi\) and \(y \le y_\eta\).
Then
\[ 0 < \frac{x}{2} \le x \le y_\xi \qandq 0 < \frac{x}{2} \le x \le y_\eta. \]
Because \(\xi\) and \(\eta\) are downward closed one has
\[ x = \frac{x}{2} + \frac{x}{2} \in \xi + \eta, \]
as desired.
When \(*\) means multiplication one has
\[ 0 < x \le y_\xi y_\eta \impliess 0 < x \inv{y_\xi} \le y_\eta \]
so that \(x\inv{y_\xi} \in \eta\).
Thus,
\[ x = y_\xi (x\inv{y_\xi}) \in \xi\eta, \]
which completes the proof that \(\xi * \eta\) is downward closed.

(iv) Let \(x = x_\xi * x_\eta \in \xi * \eta\) where \(x_\xi \in \xi\) and
\(x_\eta \in \eta\).
Because \(\xi\) does not have a greatest element there exists a
\(y_\xi \in \xi\) such that \(x_\xi < y_\xi\).
Then
\[ x < y_\xi * x_\eta \in \xi * \eta \]
showing that for all \(x \in \xi * \eta\) there is \(y \in \xi * \eta\) such
that \(x < y\), i.e. that \(\xi * \eta\) does not have a greatest element.

The definitions of addition and multiplication are compatible with the natural
structure on the power set as defined in the chapter on associative structures.
Therefore one has the following properties for all
\(\xi,\eta,\gamma \in \pos{\setR}\).
\begin{romanenum}
  \item \(\xi + \eta = \eta + \xi\),
  \item \((\xi + \eta) + \gamma = \xi + (\eta + \gamma)\),
  \item \(\xi\eta = \eta\xi\),
  \item \((\xi\eta)\gamma = \xi(\eta\gamma)\).
\end{romanenum}

\begin{proposition}
  For all \(\xi,\eta,\gamma \in \pos{\setR}\)
  \begin{romanenum}
    \item \(\xi \le \eta \impliess \xi + \gamma \le \eta + \gamma\),
    \item \(\xi \le \eta \impliess \xi\gamma \le \eta\gamma\).
  \end{romanenum}
\end{proposition}
\begin{proof}
  Let \(*\) denote addition or multiplication and let \(\xi \le \eta\).
  Let \(x \in \xi * \gamma\).
  Then \(x = x_\xi * x_\gamma\) for some \(x_\xi \in \xi\) and
  \(x_\gamma \in \gamma\).
  By assumption \(x_\xi \in \eta\) so \(x \in \eta * \gamma\).
  This shows that
  \[ \xi \le \eta \impliess \xi * \gamma \le \eta * \gamma. \qedhere \]
\end{proof}

\begin{proposition}
  For all \(\xi,\eta,\gamma \in \pos{\setR}\)
  \[ (\xi + \eta)\gamma = \xi\gamma + \eta\gamma. \]
\end{proposition}
\begin{proof}
  Suppose \(u \in (\xi + \eta)\gamma\).
  Then \(u = (x+y)z\) for some \(x \in \xi\), \(y \in \eta\) and
  \(z \in \gamma\).
  By distributivity in \(\pos{\setQ}\) one has \(u = xz + yz\) but then
  \(u \in \xi\gamma + \eta\gamma\).

  Conversely, let \(u \in \xi\gamma + \eta\gamma\).
  Then \(u = xz + yw\) for some \(x \in \xi\), \(y \in \eta\) and
  \(z,w \in \gamma\).
  Now \(\sup(z,w) = z\) or \(\sup(z,w) = w\) so \(\sup(z,w) \in \gamma\).
  Hence,
  \[ u = xz + yw \le x\sup(z,w) + y\sup(z,w) = (x+y)\sup(z,w) \]
  showing that \(u \in (\xi + \eta)\gamma\).
\end{proof}

\begin{lemma}
  Let \(q \in \pos{\setQ}\).
  The set
  \[ \xi = \{x \in \pos{\setQ} \ssep x < q\} \]
  is a positive real number.
\end{lemma}
\begin{proof}
  (i) Because \(q/2 \in \xi\) it is non-empty.

  (ii) Note that \(q\) is an upper bound of \(\xi\).
  Hence, \(\xi \subset \pos{\setQ}\).

  (iii) Let \(x \le y\) for some \(y \in \xi\).
  Then
  \[ x \le y < q \]
  so that \(x \in \xi\), showing that \(\xi\) is downward closed.

  (iv) Suppose \(x \in \xi\).
  Then
  \[ x = \frac{x + x}{2} < \frac{x + q}{2} < \frac{q + q}{2} = q \]
  showing that \(\frac{x + q}{2}\) is a strictly greater element of \(\xi\).
  Hence, \(\xi\) has no greatest element.
\end{proof}

\begin{definition}
  The number \(1 \in \pos{\setR}\) is defined as
  \[ 1 = \{x \in \pos{\setQ} \ssep x < 1\}. \]
\end{definition}

\begin{proposition}
  \(\xi \cdot 1 = \xi\) for all \(\xi \in \pos{\setR}\).
\end{proposition}
\begin{proof}
  Let \(y \in \xi \cdot 1\).
  Then \(y = xz\) for some \(x \in \xi\) and \(z \in 1\).
  Now
  \[ y = xz < x = x \]
  so that \(y \in \xi\) because \(\xi\) is downward closed.
  Therefore \(\xi \cdot 1 \subseteq \xi\).

  Conversely, suppose \(x \in \xi\).
  Because \(\xi\) has no greatest element there is \(y \in \xi\) such that
  \(y > x\).
  Now \(1 > \inv{y}x\), so \(\inv{y}x \in 1\).
  It follows that
  \[ x = y(\inv{y}x) \in \xi \cdot 1 \]
  showing \(\xi \subseteq \xi \cdot 1\).
\end{proof}

\begin{lemma}
  Let \(\xi \in \pos{\setR}\) and \(q \in \pos{\setQ}\) such that \(q > 1\).
  Then
  \[ \eta = \{qw \ssep w \in \xi\} \]
  is a positive real number such that \(\xi < \eta\).
\end{lemma}
\begin{proof}
  (i) Because \(\xi\) is non-empty, so is \(\eta\).
  (ii) \(\xi\) has an upper bound, say \(M\).
  Now \(qM\) is an upper bound for \(\eta\).
  (iii) If \(x \le y\) for some \(y \in \eta\) one has
  \[ x \le qw \impliess \inv{q}x \le w \]
  for some \(w \in \xi\) so \(\inv{q}x \in \xi\).
  Thus,
  \[ x = q\inv{q}x \in \eta. \]
  (iv) Let \(y \in \eta\).
  Then \(y = qw\) for some \(w \in \xi\).
  Because \(\xi\) has no greatest element there is \(w'\) such that \(w < w'\).
  Then
  \[ y = qw < qw' \in \eta \]
  so \(\eta\) has no greatest element.

  Let \(x \in \xi\).
  Then \(qx \in \eta\).
  Since
  \[ x < qx \]
  and \(\eta\) is downward closed \(x \in \eta\).
  Hence, \(\xi \le \eta\).
  If \(\xi = \eta\) one has
  \[
    x \in \xi
      \impliess qx \in \eta
      \impliess qx \in \xi.
  \]
  Let \(x_0 \in \xi\) be arbitrary and define \((x_k)_{n=0}^\infty\) by
  \[ x_n = q^n x_0. \]
  By construction \(x_0 \in \xi\).
  If \(x_n \in \xi\) then by the above implication one has
  \[ x_{n+1} = q^{n+1}x_0 = q q^nx_0 = qx_n \in \xi. \]
  By induction \(x_n \in \xi\) for all \(n \in \setN\).
  It shall be shown that
  \begin{equation}\tag{*}
    x_n - x_0 \ge nx_0(q - 1)
  \end{equation}
  for all \(n \in \setN\).
  For \(n = 0\) the statement is trivially true.
  Suppose \(x_n - x_0 \ge nx_0(q - 1)\).
  Then
  \[ x_{n+1} - x_n = qx_n - x_n = x_n(q-1) \ge x_0(q-1). \]
  Because the induction hypothesis gives \(x_n \ge x_0\).
  Now
  \begin{align*}
    x_{n+1} - x_0
      &= x_{n+1} - x_n + x_n - x_0 \\
      &\ge x_0(q-1) + nx_0(q-1) \\
      &= x_0(n+1)(q-1)
  \end{align*}
  so by induction (*) holds for all \(n \in \setN\).

  Let \(z\) be an upper bound of \(\eta\).
  Then \(z - x_0 > 0\).
  Since \(x_0(q-1) > 0\) the Archimedean property of the rational numbers gives
  that
  \[ z - x_0 \le nx_0(q-1) \]
  for some \(n \in \setN\), but for this \(n\) one has
  \[ z - x_0 \le x_n - x_0 \impliess z \le x_n \]
  so \(z \in \eta\) because \(x_n \in \eta\) and \(\eta\) is downward closed.
  Because \(\eta\) has no greatest element there is \(w \in \eta\) such that
  \(z < w\), but then \(z\) is not an upper bound of \(\eta\).
  This is a contradiction so the assumption that \(\xi = \eta\) must be false.
  Hence, \(\xi < \eta\).
\end{proof}

\begin{proposition}
  For all \(\xi \in \pos{\setR}\) there exists \(\eta \in \pos{\setR}\) such
  that \(\xi\eta = 1\).
\end{proposition}
\begin{proof}
  Let \(\xi \in \pos{\setR}\) be given.
  Define for each \(x \in \xi\) the positive real number
  \[
    \gamma_x = \{y \in \pos{\setQ} \ssep y < \inv{x}\}.
  \]
  Let \(z\) be an upper bound of \(\xi\) and let
  \(\lambda = \{y \in \pos{\setQ} \ssep y < \inv{z}\}\).
  Now \(x < z\) for all \(x \in \xi\), so that \(\inv{z} < \inv{x}\).
  Thus,
  \[
    y \in \lambda
      \impliess y < \inv{z}
      \impliess y < \inv{x}
      \impliess y \in \gamma_x
  \]
  for all \(x \in \xi\).
  This shows \(\lambda \le \gamma_x\) for all \(x \in \xi\), so \(\lambda\) is
  a lower bound of \(S = \{\gamma_x\}_{x \in \xi}\).
  By completeness \(S\) has a greatest lower bound, say \(\eta\).

  Let \(y \in \xi\eta\).
  Then \(y = y_\xi y_\eta\) for some \(y_\xi \in \xi\) and \(y_\eta \in \eta\).
  Because \(\eta\) is a lower bound for \(S\) it holds that
  \(\eta \le \gamma_{y_\xi}\).
  Hence, \(y_\eta < \inv{y_\xi}\).
  It now follows that
  \[ y = y_\xi y_\eta < y_\xi \inv{y_\xi} = 1 \]
  so \(y \in 1\).
  This shows that \(\xi\eta \le 1\).

  Suppose that \(\xi\eta < 1\).
  Then there is a \(c < 1\) such that \(xy < c\) for all \(x \in \xi\) and
  \(y \in \eta\).
  Let
  \[ \lambda = \left\{\frac{w}{c} \ssep w \in \eta\right\}. \]
  By the lemma \(\lambda \in \pos{\setR}\) and \(\eta < \lambda\).
  Suppose \(z \in \lambda\) and let \(x \in \xi\).
  Then
  \[ xz = \frac{xy}{c} < \frac{c}{c} = 1 \]
  for some \(y \in \eta\), but then \(z < \inv{x}\) for all \(x \in \xi\).
  This shows that \(\lambda \le \gamma_x\) for all \(x\), but this contradicts
  that \(\eta\) is the greatest lower bound of \(S\).
  Thus, the assumption that \(\xi\eta < 1\) is false, and it follows that
  \(\xi\eta = 1\).
\end{proof}

Precisely as in the chapter on associative structures one shows that the inverse
is unique.
Thus, the \(\eta\) in the proposition can be denoted by \(\inv{\xi}\).

\begin{lemma}
  If \(\xi,\eta \in \pos{\setR}\) and \(\xi < \eta\) then there is a
  \(y \in \eta\) that \(x < y\) for all \(x \in \xi\).
\end{lemma}
\begin{proof}
  The assumption gives \(\xi \subset \eta\).
  Let \(y \in \eta \setminus \xi\).
  If \(y \le x\) for some \(x \in \xi\), then \(y \in \xi\) by downward
  closedness, which is contradicts the construction.
  Hence, \(x < y\) for all \(x \in \xi\).
\end{proof}
\begin{lemma}
  For all \(\xi,\eta \in \pos{\setR}\)
  \[ \xi < \eta \impliess \xi + 1 < \eta + 1. \]
\end{lemma}
\begin{proof}
  Let \(\xi < \eta\).
  Then there is a \(y \in \eta\) such that \(x < y\) for all \(x \in \xi\).
  Then
  \[ x + 1 < y + 1 \]
  for all \(x \in \xi\).
  Since \(\eta\) has no greatest element there is a \(w \in \eta\) such that
  \(w > y\).
  If \(w \ge y + 1\) one has
  \[ x + z < x + 1 < y + 1 \le w < w + \frac{1}{2} \in \eta + 1 \]
  for all \(x \in \xi\) and \(z \in 1\).
  Hence, \(w + \frac{1}{2} \notin \xi + 1\) showing that \(\xi + 1 < \eta + 1\).
  Otherwise \(0 < w - y < 1\).
  Let \(c = 1 - (w-y)\).
  Then \(0 < c < 1\) so that \(c \in 1\).
  Now
  \[ x + z < x + 1 < y + 1 = w + (1 - (w - y)) = w + c \in \eta + 1 \]
  for all \(x \in \xi\) and \(z \in 1\).
  Therefore \(w + c \notin \xi + 1\) showing that
  \(\xi + 1 < \eta + 1\).
\end{proof}
\begin{proposition}
  For all \(\xi,\eta,\gamma \in \pos{\setR}\)
  \[ \xi + \gamma = \eta + \gamma \impliess \xi = \eta. \]
\end{proposition}
\begin{proof}
  Suppose \(\xi + \gamma = \eta + \gamma\).
  Now there is \(\gamma'\) such that \(\gamma\gamma' = 1\).
  It follows that
  \[
    \xi\gamma' + 1
      = \xi\gamma' + \gamma\gamma'
      = (\xi + \gamma)\gamma'
      = (\eta + \gamma)\gamma'
      = \eta\gamma' + \gamma\gamma'
      = \eta\gamma' + 1.
  \]
  By trichotomy and the lemma it must hold that \(\xi\gamma' = \eta\gamma'\).
  Therefore
  \[ \xi = \xi\gamma'\gamma = \eta\gamma'\gamma = \eta \]
  as desired.
\end{proof}

This completes the construction of the positive reals.
\begin{theorem}
  For all \(\xi,\eta,\gamma \in \pos{\setR}\) the following laws hold.
  \begin{romanenum}
    \item \(\xi + \eta = \eta + \xi\).
    \item \((\xi + \eta) + \gamma = \xi + (\eta + \gamma)\).
    \item \(\xi + \gamma = \eta + \gamma \impliess \xi = \eta\).
    \item \(\xi\eta = \eta\xi\).
    \item \((\xi\eta)\gamma = \xi(\eta\gamma)\).
    \item \(\xi \cdot 1 = \xi\).
    \item There exists \(\inv{\xi}\) such that \(\xi\inv{\xi} = 1\).
    \item \((\xi + \eta)\gamma = \xi\gamma + \eta\gamma\).
    \item \(\xi \le \eta\) or \(\eta \le \xi\).
    \item If \(\xi \le \eta\) and \(\eta \le \xi\), then \(\xi = \eta\).
    \item If \(\xi \le \eta\) and \(\eta \le \gamma\), then \(\xi \le \gamma\).
    \item If \(\xi \le \eta\), then \(\xi + \gamma \le \eta + \gamma\).
    \item If \(\xi \le \eta\), then \(\xi\gamma \le \eta\gamma\).
    \item
      Every non-empty of subset \(\pos{\setR}\) with an upper bound has a least
      upper bound.
  \end{romanenum}
\end{theorem}

The next lemma is quite intuitive, but has not yet been shown.
\begin{lemma}
  \(\xi < \xi + \eta\) for all \(\xi,\eta \in \pos{\setR}\).
\end{lemma}
\begin{proof}
  Fix a \(y \in \eta\).
  Then \(x < x + y\) for all \(x \in \xi\), but the right hand side belongs to
  \(\xi + \eta\) so \(x \in \xi + \eta\) by downward closedness.
  This shows that \(\xi \le \xi + \eta\).
  If \(\xi = \xi + \eta\) then for each \(x \in \xi\) one has \(x + y \in \xi\).
  Take any \(x \in \xi\) and define the sequence \((x_n)_{n=0}^\infty\) such
  that
  \[ x_n = x + ny. \]
  Clearly \(x_0 \in \xi\).
  If \(x_n \in \xi\) then
  \[ x_{n+1} = x + (n+1)y = x + ny + y = x_n + y \in \xi \]
  by assumption.
  By induction it follows that \(x_n \in \xi\) for all \(n\).
  By definition \(\xi\) has an upper bound, say \(z\).
  Now \(x < z\) so there is an \(n \in \setN\) such that
  \[ 0 < z - x < ny \]
  by the Archimedean property, but then
  \[ 0 < z < x + ny = x_n \in \xi \]
  contradicting that \(z\) is an upper bound.
  This means the assumption that \(\xi = \xi + \eta\) is false, s
   \(\xi < \xi + \eta\).
\end{proof}

The next result is in fact a quite fundamental result, so the proof is rather
involved.

\begin{lemma}
  For all \(\xi,\eta \in \pos{\setR}\) such that \(\xi < \eta\) there exists
  a \(\gamma \in \pos{\setR}\) such that \(\xi + \gamma < \eta\).
\end{lemma}
\begin{proof}
  Suppose \(\xi < \eta\) with \(\xi,\eta \in \pos{\setR}\).
  Now there is \(y \in \eta\) such that \(x < y\) for all \(x \in \xi\).
  Because \(\eta\) has no greatest element there is \(y' \in \eta\) such that
  \(y < y'\).
  By the Archimedean property there is \(n \in \pos{\setZ}\) such that
  \[ 0 < \frac{1}{n} < \min(y-x,y' - y). \]
  Because \(\xi\) has an upper bound there must exist an \(m \in \pos{\setZ}\)
  such that
  \[ \frac{m}{n} \notin \xi. \]
  Let \(m\) be the least such integer.
  Let \(p\) be the least positive integer such that
  \[ y < \frac{p}{n}. \]

  \(p/n \in \xi\) leads to the contradiction that
  \(y \in \xi\), so \(p/n \notin \xi\).
  If \(p < m\) then
  \[ x < y < \frac{p}{n} < \frac{m}{n} \]
  contradicts the minimality of \(m\).
  Hence, \(m \le p\).
  If \(m = p\)
  \[
    \frac{1}{n} < y - x \impliess x + \frac{1}{n} < y < \frac{m}{n}
    \impliess
    x < \frac{m-1}{n}
  \]
  again contradicting the minimality of \(m\).
  This shows that \(m < p\).
  Because \(p\) is the least positive integer such that \(y < p/n\) it follows
  that
  \[ \frac{m}{n} \le y. \]
  If \(y' \le p/n\) one has
  \[
    \frac{1}{n} < y' - y
    \impliess
    y + \frac{1}{n} < y' \le \frac{p}{n}
    \impliess
    y < \frac{p-1}{n}
  \]
  contradicting the minimality of \(p\).
  This completes showing the chain of inequalies
  \[ x < \frac{m}{n} \le y < \frac{p}{n} < y' \]
  for all \(x \in \xi\).

  One has \(\frac{p-m}{n} \in \pos{\setQ}\).
  Define the positive real number
  \[ \gamma = \left\{w \in \pos{\setQ} \ssep w < \frac{p-m}{n}\right\}. \]
  Suppose \(z \in \xi + \gamma\).
  Then \(z = z_\xi + z_\gamma\) for some \(z_\xi \in \xi\) and
  \(z_\gamma \in \gamma\).
  Now
  \[ z = z_\xi + z_\gamma < \frac{m}{n} + \frac{p-m}{n} = \frac{p}{n} < y' \]
  so \(z \in \eta\).
  This shows that \(\xi + \gamma < \eta\).
\end{proof}

With this lemma the next property of the positive reals can be shown.
This property is shared with the natural numbers and is important in introducing
all real numbers in the next section.

\begin{proposition}
  If \(\xi < \eta\) for some \(\xi,\eta \in \pos{\setR}\), then there exists
  \(\gamma \in \pos{\setR}\) such that \(\xi + \gamma = \eta\).
\end{proposition}
\begin{proof}
  Let
  \[ S = \{\omega \in \pos{\setR} \ssep \xi + \omega \le \eta\}. \]
  By the lemma \(S\) is non-empty.
  Furthermore \(\eta\) is an upper bound for \(S\) since
  \[ \eta < \xi + \eta. \]
  It follows by completeness that \(S\) has a least upper bound, say \(\gamma\).
  If \(\xi + \gamma < \eta\), there is by the lemma an \(\omega\) such that
  \[ \xi + \gamma + \omega < \eta, \]
  but then \(\gamma + \omega\) is an element in \(S\) strictly greater than
  \(\gamma\) contradicting that \(\gamma\) is an upper bound of \(S\).
  Suppose \(\xi + \gamma > \eta\).
  Then there is \(y \in \xi + \gamma\) such that \(x < y\) for all
  \(x \in \eta\).
  Let \(y = y_\xi + y_\gamma\).
  Take a \(z \in \gamma\) such that \(y_\gamma < z\).
  Define the positive real numbers
  \[
    \omega = \{w \in \pos{\setQ} \ssep w < z\}.
  \]
  Now \(\omega < \gamma\) because \(z \notin \omega\) but \(z \in \gamma\).
  One has
  \[ \eta < \xi + \omega < \xi + \gamma \]
  since \(y = y_\xi + y_\gamma\) belongs to the left hand side and
  \(y \notin \eta\).
  This shows that \(\omega\) is a smaller upper bound of \(S\) than \(\gamma\)
  which contradicts that \(\gamma\) is the least upper bound of \(S\).
  Therefore it must hold that \(\xi + \gamma = \eta\).
\end{proof}

\subsection{Real Numbers}

Note that \((\pos{\setR},+)\) is a commutative cancelative semigroup.
Define an equivalence relation on \(\pos{\setR}^2\) by
\[ (\xi_1,\xi_2) \sim (\eta_1,\eta_2) \iffs \xi_1 + \eta_2 = \xi_2 + \eta_1. \]
By lemma \ref{lem:assoc-group-from-semigroup} this relation is an equivalence
relation.
Denote the set of equivalence classes by \(\setR\).
By the same lemma \((\setR,+)\) is a group under
\[ (\xi_1,\xi_2) + (\eta_1,\eta_2) = (\xi_1+\eta_1,\xi_2+\eta_2). \]
As in the section introducing integers define multiplication by
\[
  [(\xi_1,\xi_2)][(\eta_1,\eta_2)]
  =
  [(\xi_1\eta_1 + \xi_2\eta_2, \xi_1\eta_2 + \xi_2\eta_1)].
\]
Furthermore the relation \(\le\) on \(\setR\) is defined as
\[
  [(\xi_1,\xi_2)] \le [(\eta_1,\eta_2)]
  \iffs
  \xi_1 + \eta_2 \le \xi_2 + \eta_1.
\]

Define
\[ 0 \defeq [(\xi,\xi)] \]
where \(\xi\) is an arbitrary element of \(\pos{\setR}\).
The definition is good because for all \(\xi,\eta \in \pos{\setR}\) one has
\[ (\xi,\xi) \sim (\eta,\eta) \iffs \xi + \eta = \xi + \eta. \]

In the section on integers the number \(1\) was defined as \(1 = [(1,0)]\).
However, there is no \(0 \in \pos{\setR}\)!
Therefore another definition is necessary.
Define \(2 \in \pos{\setR}\) such that \(2 = 1 + 1\) and define
\[ 1 \defeq [(2,1)]. \]

To distinguish the real numbers, which are equivalence classes of ordered pairs,
from the positive reals, which are dedekind cuts, the symbols \(\XI, \ETA\) and
\(\GAMMA\) will be used.

By the group properties and by essentially the same proofs as in the section on
integers one gets the following properties for all
\(\XI, \ETA, \GAMMA \in \setR\).
\begin{romanenum}
  \item \(\XI + \ETA = \ETA + \XI\).
  \item \((\XI + \ETA) + \GAMMA = \XI + (\ETA + \GAMMA)\).
  \item \(\XI + 0 = \XI\).
  \item There exists \(-\XI \in \setR\) such that \(\XI + (-\XI) = 0\).
  \item \(\XI\ETA = \ETA\XI\).
  \item \((\XI\ETA)\GAMMA = \XI(\ETA\GAMMA)\).
  \item \(\XI \cdot 1 = \XI\).
  \item \((\XI + \ETA)\GAMMA = \XI\GAMMA + \ETA\GAMMA\).
  \item \(\XI \le \ETA\) or \(\ETA \le \XI\).
  \item If \(\XI \le \ETA\) and \(\ETA \le \XI\), then \(\XI = \ETA\).
  \item If \(\XI \le \ETA\) and \(\ETA \le \GAMMA\), then \(\XI \le \GAMMA\).
  \item If \(\XI \le \ETA\), then \(\XI + \GAMMA \le \ETA + \GAMMA\).
  \item
    If \(\XI \le \ETA\) and \(\GAMMA \ge 0\), then \(\XI\GAMMA \le \ETA\GAMMA\).
\end{romanenum}

There are only two properties remaining.
It shall be shown that \(\pos{\setR}\) is embedded in \(\setR\) from which the
remaining properties can easily be deduced.
Let \(\xi \in \pos{\setR}\) be arbitrary and define
\[
  \varphi_\xi: \pos{\setR} \ni \omega \mapsto [(\xi + \omega,\xi)] \in \setR.
\]
Now let \(\eta \in \pos{\setR}\).
For all \(\omega \in \pos{\setR}\) one has
\[
  \xi + \omega + \eta = \xi + \eta + \omega
  \impliess
  (\xi + \omega, \xi) \sim (\eta + \omega, \eta)
  \impliess
  \varphi_\xi(\omega) = \varphi_\eta(\omega).
\]
This shows that the definition of \(\varphi: \pos{\setR} \to \setR\) by
\[ \varphi(\omega) = \varphi_\xi(\omega) \]
for some \(\xi \in \pos{\setR}\) is good.
\(\varphi\) is injective because
\begin{align*}
  \varphi(\omega_1) = \varphi(\omega_2)
    &\impliess [(\xi + \omega_1,\xi)] = [(\xi + \omega_2,\xi)] \\
    &\impliess (\xi + \omega_1,\xi) \sim (\xi + \omega_2, \xi) \\
    &\impliess \xi + \omega_1 + \xi = \xi + \xi + \omega_2 \\
    &\impliess \omega_1 = \omega_2.
\end{align*}
Furthermore \(\varphi\) respects addition because
\begin{align*}
  \varphi(\gamma + \omega)
    &= [(\xi + \gamma + \omega,\xi)] \\
    &= [(\xi + \gamma + \xi + \omega, \xi + \xi)] \\
    &= [(\xi + \gamma,\xi)] + [(\xi + \omega, \xi)] \\
    &= \varphi(\gamma) + \varphi(\omega).
\end{align*}
By considering \(\varphi = \varphi_{\xi^2 + \xi\gamma + \xi\omega + \xi^2}\)
one gets that \(\varphi\) respects multiplication, as follows
\begin{align*}
  \varphi(\gamma\omega)
    &= [(\xi^2 + \xi\gamma + \xi\omega + \gamma\omega + \xi^2,
         \xi^2 + \xi\gamma + \xi^2 + \xi\omega)] \\
    &= [((\xi + \gamma)(\xi + \omega) + \xi^2,
         (\xi + \gamma)\xi + \xi(\xi + \omega))] \\
    &= [(\xi + \gamma,\xi)][(\xi + \omega,\xi)] \\
    &= \varphi(\gamma)\varphi(\omega).
\end{align*}
Lastly \(\varphi\) respects the order, since
\begin{align*}
  \gamma \le \omega
    &\iffs \xi + \gamma + \xi \le \xi + \xi + \omega \\
    &\iffs [(\xi + \gamma,\xi)] \le [(\xi + \omega, \xi)] \\
    &\iffs \varphi(\gamma) \le \varphi(\omega).
\end{align*}
This shows that \(\varphi\) is an embedding.
Consider the set
\[ \pos{\setR^\star} \defeq \{\XI \in \setR \ssep \XI > 0\}. \]
Let \(\XI = [(\xi_1,\xi_2)] \in \pos{\setR^\star}\).
Then
\[ \xi_1 + \gamma > \xi_2 + \gamma \]
for some \(\gamma\), but then \(\xi_1 = \xi_2 + \xi\) for some
\(\xi \in \pos{\setR}\).
Now
\[ \varphi(\xi) = [(\xi_2 + \xi, \xi_2)] = [(\xi_1,\xi_2)] = \XI \]
so that \(\varphi\) is an isomorphism \(\pos{\setR} \to \pos{\setR^\star}\).

\begin{proposition}
  For all \(\XI \in \setR\) such that \(\XI \ne 0\) there exists
  \(\ETA \in \setR\) such that \(\XI\ETA = 1\).
\end{proposition}
\begin{proof}
  If \(\XI \ne 0\) then either \(\XI > 0\) or \(\XI < 0\).
  In the first case \(\XI \in \pos{\setR^\star}\) so that
  \[ \varphi(\xi) = \XI \]
  for some \(\xi \in \pos{\setR}\).
  Let \(\eta \in \pos{\setR}\) such that \(\xi\eta = 1\).
  With \(\ETA = \varphi(\eta)\) one now has
  \[ \XI\ETA = \varphi(\xi)\varphi(\eta) = \varphi(\xi\eta) = \varphi(1) = 1. \]
  In the other case \(-\XI > 0\) so there is an \(\ETA \in \setR\) such that
  \[ \XI(-\ETA) = (-\XI)\ETA = 1 \]
  which completes the proof.
\end{proof}

\begin{proposition}
  Every non-empty subset of \(\setR\) with an upper bound has a least upper
  bound in \(\setR\).
\end{proposition}
\begin{proof}
  Let \(S \subseteq \setR\) be non-empty with an upper bound in \(\setR\),
  say \(\GAMMA_S\).
  Let \(\ETA\) be an arbitrary element of \(S\) and define
  \(f: \setR \to \setR\) by
  \[ f(\XI) = \XI - \ETA + 1. \]
  Clearly \(f\) is an order preserving bijection, and consequently order
  isomorphism.
  By lemma \ref{lem:order-lattice-completeness} \(S\) has a least upper bound
  if and only if
  \[ T \defeq f(S) \]
  has a least upper bound.
  Now
  \[ \GAMMA_T \defeq f(\GAMMA_S) \]
  is an upper bound of \(T\).

  Define
  \[ \pos{T} = \{\XI \in T \ssep \XI > 0\}. \]
  Since
  \[ 0 < 1 = \ETA - \ETA + 1 = f(\ETA) \in T \]
  \(\pos{T}\) is non-empty.
  Now
  \[ \XI \in \pos{T} \impliess \XI \in T \impliess \XI \le \GAMMA_T \]
  showing that \(\GAMMA_T\) is an upper bound of \(\pos{T}\).
  Note that \(\varphi: \pos{\setR} \to \pos{\setR^\star}\) is an order
  isomorphism.
  \(\varphi(\pos{T}) \subseteq \pos{\setR}\) is a non-empty subset with an
  upper bound so that it has a least upper bound.
  By lemma \ref{lem:order-lattice-completeness} \(\pos{T}\) has a least upper
  bound, say \(\OMEGA\).

  Suppose \(\XI \in T\).
  If \(\XI > 0\) then \(\XI \in \pos{T}\) so \(\XI \le \OMEGA\).
  Otherwise,
  \[ \XI \le 0 < \OMEGA \]
  showing that \(\OMEGA\) is an upper bound of \(T\).
  If \(\OMEGA'\) is an upper bound of \(T\) then in particular
  \[ \XI \le \OMEGA' \]
  for all \(\XI \in \pos{T}\) showing that \(\OMEGA'\) is an upper bound of
  \(\pos{T}\).
  Hence, \(\OMEGA \le \OMEGA'\) showing that \(\OMEGA\) is in fact the least
  upper bound of \(T\).
  Hence, \(S\) has a least upper bound.
\end{proof}

Now all properties of \(\setR\) are in place, and the existence of a complete
totally ordered field has been established.
The properties are listed in the next theorem, in which the funky symbols have
been replaced by the more natural \(x,y,z\).

\begin{theorem}
  For all \(x,y,z \in \setR\) the following laws hold.
  \begin{romanenum}
    \item \(x + y = y + x\).
    \item \((x + y) + z = x + (y + z)\).
    \item \(x + 0 = x\).
    \item There exists \(-x\) such that \(x + (-x) = 0\).
    \item \(xy = yx\).
    \item \((xy)z = x(yz)\).
    \item \(x \cdot 1 = x\).
    \item There exists \(\inv{x}\) such that \(x\inv{x} = 1\).
    \item \((x + y)z = xz + yz\).
    \item \(x \le y\) or \(y \le x\).
    \item If \(x \le y\) and \(y \le x\), then \(x = y\).
    \item If \(x \le y\) and \(y \le z\), then \(x \le z\).
    \item If \(x \le y\), then \(x + z \le y + z\).
    \item If \(x \le y\), then \(xz \le yz\).
    \item
      Every non-empty of subset \(\setR\) with an upper bound has a least upper
      bound.
  \end{romanenum}
\end{theorem}

In the next section it will be shown that any system fulfilling these 15
properties is unique up to isomorphism.
Hence, it is possible to forget all about Dedekind cuts and use this theorem
as an axiom system for the real numbers instead, which is what most people do.

\subsection{Uniqueness}

FIXME
