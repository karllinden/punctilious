\section{Symbolic Boolean Algebra}

Before introducing the logic needed to lay the foundation of mathematics the
related topic of symbolic boolean algebras are studied.
The reader should be aware that in this section everything is essentially just
\emph{symbolic}, thereof the name\footnote{The term ``symbolic boolean algebra''
is invented by the author with inspiration from the established term ``boolean
algebra''.
The author of this text does not know if anyone else has used the term.}.
Hence, nothing in this section has any meaning until any meaning has been
assigned to the symbols.

The concepts of this section will be applied to logic, set algebra and
\emph{actual} boolean algebras, which are founded in set theory.
These applications make it worthwhile to treat the subject in its own right.

\begin{definition}
  A symbolic boolean algebra, \(\mathcal{B}\), is an undefined object which
  contains expressions, which are denoted by letters in the alphabet
  \[ a,b,c,\dots,x,y,z,w. \]
  If an expression \(x\) belongs to \(\mathcal{B}\), then one writes
  \(x \in \mathcal{B}\).
  Two expressions \(x\) and \(y\) in \(\mathcal{B}\) can be equal in which case
  one writes \(x = y\).
  The equality fulfills the following axioms
  \begin{romanenum}
    \item
    \(x = x\).
    \item
    If \(x = y\), then \(y = x\).
    \item
    If \(x = y\) and \(y = z\), then \(x = z\).
  \end{romanenum}

  In a symbolic boolean algebra \(\mathcal{B}\) there are symbols \(\wedge\),
  \(\vee\) and \(\neg\) such that if \(x \in \mathcal{B}\) and
  \(y \in \mathcal{B}\) then
  \[ (x \vee y), \quad (x \wedge y) \quad \text{and} \quad (\neg x) \]
  are expressions of \(\mathcal{B}\).
\end{definition}

When there is no ambiguity the parentheses will be omitted, so that the for
example expressions \((x \wedge y)\) and \(x \wedge y\) are the same.
Also \(\neg\) shall be evaluated before anything else, so that for example
\(\neg x \vee y\) means \((\neg x) \vee y\).

\begin{named}[Symbolic axioms]
  Expressions \(x\), \(y\) and \(z\) of a symbolic boolean algebra
  \(\mathcal{B}\) fulfill the following axioms.
  \begin{enumerate}[twocol]
    \item
    \(x \vee y = y \vee x\).
    \item
    \(x \vee (y \vee z) = (x \vee y) \vee z\).
    \item
    \(x \vee (y \wedge z) = (x \vee y) \wedge (x \vee z)\).
    \item
    \(x \wedge y = y \wedge x\).
    \item
    \(x \wedge (y \wedge z) = (x \wedge y) \wedge z\).
    \item
    \(x \wedge (y \vee z) = (x \wedge y) \vee (x \wedge z)\).
    \setcounter{enumTemp}{\theenumi}
  \end{enumerate}
  There exists \(0,1 \in \mathcal{B}\) such that the following axioms hold.
  \begin{enumerate}[twocol]
    \setcounter{enumi}{\theenumTemp}
    \item
    \(x \vee 0 = x\).
    \item
    \(x \vee \neg x = 1\).
    \item
    \(x \wedge 1 = x\).
    \item
    \(x \wedge \neg x = 0\).
  \end{enumerate}
\end{named}

\begin{definition}
  The dual \(x^d\) of the expression \(x\) is the expression which is acquired
  by swapping all occurrences of \(\vee\) with \(\wedge\) and likewise for \(0\)
  and \(1\) in \(x\).
\end{definition}

The following is the principle of duality which in some sense cannot proved
stringently.
Anyway an argumentation why it must be true is given as ``proof''.

\begin{named}[Principle of duality]
  Let \(x\) and \(y\) be expressions in \(\mathcal{B}\), such that \(x = y\).
  Then \(x^d = y^d\).
\end{named}
\begin{proof}
  Suppose that \(x = y\) has been proven for any symbolic boolean algebra.
  Since the axioms are symmetric, it is seen by inspection that by swapping
  \(\vee\) and \(0\) with \(\wedge\) and \(1\), then one acquires the same
  symbolic boolean algebra, but in which the dual equality is proven.
  Thus, also \(x^d = y^d\) is proven.
\end{proof}

\begin{footnotesize}
  The reader who feels uncomfortable with this principle should try all proofs
  by exchanging \(\vee\) for \(\wedge\) and \(0\) for \(1\).
\end{footnotesize}

Using the axioms one can now deduce other equalities.
By the principle of duality one only needs to prove (about) every other
proposition.

\begin{proposition}
  \(\neg 0 = 1\).
\end{proposition}
\begin{proposition}
  \(\neg 1 = 0\).
\end{proposition}
\begin{proof}
  \(\neg 0 = \neg 0 \vee 0 = 0 \vee \neg 0 = 1\).
\end{proof}

\begin{proposition}
  \(x \vee 1 = 1\).
\end{proposition}
\begin{proposition}
  \(x \wedge 0 = 0\).
\end{proposition}
\begin{proof}
  \(
    x \vee 1
      = (x \vee 1) \wedge 1
      = (x \vee 1) \wedge (x \vee \neg x)
      = x \vee (1 \wedge \neg x)
      = x \vee \neg x
      = 1.
  \)
\end{proof}

The following propositions are called the absorption laws, which are sometimes
included in the axioms, although they can be proven from the stated axioms.
\begin{proposition}
  \(x \vee (x \wedge y) = x\).
\end{proposition}
\begin{proposition}
  \(x \wedge (x \vee y) = x\).
\end{proposition}
\begin{proof}
  \(
    x \vee (x \wedge y)
      = (x \wedge 1) \vee (x \wedge y)
      = x \wedge (1 \vee y)
      = x.
  \)
\end{proof}

Using the absorption laws the next propositions are immediate.

\begin{proposition}
  \(x \wedge x = x\).
\end{proposition}
\begin{proposition}
  \(x \vee x = x\).
\end{proposition}
\begin{proof}
  \(x \vee x = x \vee (x \wedge 1) = x\).
\end{proof}

The next two propositions, which are similar to the absorption laws, follow by
\[
  \begin{split}
    x \vee (\neg x \wedge y)
      &= x \vee (x \wedge y) \vee (\neg x \wedge y) \\
      &= x \vee ((x \vee \neg x) \wedge y) \\
      &= x \vee (1 \wedge y) \\
      &= x \vee y.
  \end{split}
\]
\begin{proposition}
  \(x \vee (\neg x \wedge y) = x \vee y\).
\end{proposition}
\begin{proposition}
  \(x \wedge (\neg x \vee y) = x \wedge y\).
\end{proposition}

\begin{proposition}
  \(\neg \neg x = x\).
\end{proposition}
\begin{proof}
  The result is acquired by a symmetric computation.
  \[
    \begin{split}
      \neg \neg x
        &= \neg \neg x \vee 0 \\
        &= \neg \neg x \vee (x \wedge \neg x) \\
        &= (\neg \neg x \vee x) \wedge (\neg \neg x \vee \neg x) \\
        &= (\neg \neg x \vee x) \wedge (\neg x \vee \neg (\neg x)) \\
        &= (\neg \neg x \vee x) \wedge 1 \\
        &= (x \vee \neg \neg x) \wedge 1 \\
        &= (x \vee \neg \neg x) \wedge (x \vee \neg x) \\
        &= x \vee (\neg \neg x \wedge \neg x) \\
        &= x \vee (\neg x \wedge \neg (\neg x)) \\
        &= x \vee 0 \\
        &= x.
      \qedhere
    \end{split}
  \]
\end{proof}

\begin{proposition}
  If \(x \wedge y = x \wedge z = 0\) and \(x \vee y = x \vee z = 1\), then
  \(y = z\).
\end{proposition}
\begin{proof}
  \[
    \begin{split}
      y
        &= y \wedge 1 \\
        &= y \wedge (x \vee z) \\
        &= (y \wedge x) \vee (y \wedge z) \\
        &= (x \wedge y) \vee (y \wedge z) \\
        &= 0 \vee (y \wedge z) \\
        &= (x \wedge z) \vee (y \wedge z) \\
        &= (x \vee y) \wedge z \\
        &= 1 \wedge z \\
        &= z.
      \qedhere
    \end{split}
  \]
\end{proof}
\begin{corollary}
  If \(x \wedge y = 0\) and \(x \vee y = 1\), then \(y = \neg x\).
\end{corollary}

The following propositions are called De Morgan's laws.

\begin{proposition}
  \(\neg(x \vee y) = \neg x \wedge \neg y\).
\end{proposition}
\begin{proposition}
  \(\neg(x \wedge y) = \neg x \vee \neg y\).
\end{proposition}
\begin{proof}
  Firstly
  \[
    \begin{split}
      (x \vee y) \wedge (\neg x \wedge \neg y)
        &= ((x \vee y) \wedge \neg x) \wedge ((x \vee y) \wedge \neg y) \\
        &=
          ((x \wedge \neg x) \vee (y \wedge \neg x))
          \wedge
          ((x \wedge \neg y) \vee (y \wedge \neg y)) \\
        &=
          (0 \vee (y \wedge \neg x))
          \wedge
          ((x \wedge \neg y) \vee 0) \\
        &= (y \wedge \neg x) \wedge (x \wedge \neg y) \\
        &= (x \wedge \neg x) \wedge (y \wedge \neg y) \\
        &= 0 \wedge 0 \\
        &= 0.
    \end{split}
  \]
  Secondly
  \[
    \begin{split}
      (x \vee y) \vee (\neg x \wedge \neg y)
        &= ((x \vee y) \vee \neg x) \wedge (x \vee y \vee \neg y) \\
        &= ((x \vee \neg x) \vee y) \wedge (x \vee (y \vee \neg y)) \\
        &= (1 \vee y) \wedge (x \vee 1) \\
        &= 1 \wedge 1 \\
        &= 1.
    \end{split}
  \]
  Therefore by the uniqueness of \(\neg (x \vee y)\) one has
  \[ \neg (x \vee y) = \neg x \wedge \neg y. \qedhere \]
\end{proof}
