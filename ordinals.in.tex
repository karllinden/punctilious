\section{Ordinals}

The ordinal numbers constitute a direct generalization of the natural numbers.
In fact, the construction is the same, but before being concerned with existence
the fundamental facts of ordinals will be shown.
The reason for even dealing with ordinals is their importance in the proof of
Zorn's Lemma, which is indispensable.

\begin{definition}
  A set \(A\) is said to be transitive if whenever \(x \in y\) and \(y \in A\),
  then \(x \in A\), i.e.
  \[ y \in A \wedge x \in y \impliess x \in A. \]
\end{definition}

\begin{proposition}
  \(A\) is transitive iff
  \[ y \in A \impliess y \subseteq A. \]
\end{proposition}
\begin{proof}
  Let \(A\) be transitive and let \(y \in A\).
  Suppose \(x \in y\).
  Then since \(A\) is transitive \(x \in y\), but this shows \(y \subseteq A\).

  Contrarily, let the condition in the proposition hold.
  Suppose \(x \in y\) and \(y \in A\).
  By assumption \(y \subseteq A\), so that \(x \in A\), which means \(A\) is
  transitive.
\end{proof}

The following definition is due to John von Neumann.

\begin{definition}
  An ordinal is a transitive set which is strictly well-ordered under \(\in\).
\end{definition}

For \(\alpha = \varnothing\) the requirements of the definition are vacuously
true.
Therefore on has the following proposition.
\begin{proposition}
  \(\varnothing\) is an ordinal.
\end{proposition}
\begin{definition}
  The zero ordinal is defined as \(0 \defeq \varnothing\).
\end{definition}

\begin{definition}
  If \(\alpha\) is an ordinal, then the successor, \(\alpha + 1\) is defined by
  \(\{\alpha\} \cup \alpha\).
\end{definition}

\begin{lemma}
  For all sets \(x\) and \(y\) at most one of \(x \in y\), \(x = y\) and
  \(y \in x\) can hold.
\end{lemma}
\begin{proof}
  By proposition \ref{prop:set-x-notin-x} neither of \(x \in y\) nor \(y \in x\)
  can occur if \(x = y\).
  That \(x \in y\) and \(y \in x\) cannot occur simultaneously follows from
  proposition \ref{prop:set-not-x-in-y-and-y-in-x}.
\end{proof}

\begin{proposition}
  If \(\alpha\) is an ordinal, then \(\alpha + 1\) is an ordinal.
\end{proposition}
\begin{proof}
  First it shall be shown that \(\alpha + 1\) is transitive.
  Let \(y \in \alpha + 1\).
  Then by definition either \(y = \alpha\) or \(y \in \alpha\).
  In the first case not that \(y = \alpha\) implies \(y \subseteq \alpha\).
  In the second case \(y \subseteq \alpha\) follows by the assumption on
  \(\alpha\).
  Now transitivity follows in both cases from
  \[ y \subseteq \alpha \subseteq \alpha \cup \{\alpha\} = \alpha + 1. \]

  It must be shown that \(\alpha + 1\) is well-ordered under \(\in\).
  Suppose \(x,y \in \alpha + 1\) and \(x \ne y\).
  If both of \(x,y \in \alpha\) then precisely one of \(x \in y\), \(x = y\) and
  \(y \in x\) can occur by totality of \(<\) on \(\alpha\).
  Otherwise it can without loss of generality be assumed that \(x = \alpha\) and
  \(y \in \alpha\).
  Then \(y \in x\).
  By the lemma exactly one of \(x \in y\), \(x = y\) and \(y \in x\) holds for
  all \(x,y \in \alpha\).
  This shows condition (i) in proposition \ref{prop:ord-well-ordered}.

  Let \(S \subseteq \alpha + 1\) be non-empty.
  Let \(A = S \cap \alpha\) and \(B = S \cap \{\alpha\}\).
  Then
  \[ A \cup B = (S \cap \alpha) \cup (S \cap \{\alpha\}) = S. \]
  If \(A \ne \varnothing\) then by assumption on \(\alpha\), \(A\) has a least
  element, say \(x\).
  If \(y \in S\) then \(y \in A\) or \(y \in B\).
  In the first case \(x \in y\) or \(x = y\).
  In the second case \(x \in \alpha\) and \(y = \alpha\) so that \(x \in y\).
  In any case \(x\) is the least element of \(S\).
  Otherwise, if \(A = \varnothing\) then \(S = \{\alpha\}\) and \(\alpha\) is
  the least element of \(S\).
  By proposition \ref{prop:ord-well-ordered} \(\alpha + 1\) is well-ordered
  completing the proof.
\end{proof}

\begin{proposition}
  If \(\alpha\) is an ordinal and \(\beta \in \alpha\), then \(\beta\) is an
  ordinal.
\end{proposition}
\begin{proof}
  Suppose \(y \in \beta\) and \(x \in y\).
  By transitivity of \(\alpha\) one has \(y \in \alpha\).
  Transitivity again gives \(x \in \alpha\).
  By totality of the strict order on \(\alpha\) one has \(x \in \beta\),
  \(x = \beta\) or \(\beta \in x\).
  \(x = \beta\) is a contradiction.
  Due to the order being strict the latter is also a contradiction since
  \[ \beta \in x \in y \in \beta \]
  is a contradiction.
  Thus, \(x \in \beta\) and \(\beta\) is transitive.

  It shall be shown that \(\beta\) is well-ordered under \(\in\).
  Let \(x,y \in \beta\).
  By transitivity of \(\alpha\) one has \(x,y \in \alpha\).
  Since \(\in\) is a strict total order on \(\alpha\) one has precisely one of
  \(x \in y\), \(x = y\) and \(y \in x\).
  This shows condition (i) in proposition \ref{prop:ord-well-ordered}.

  Let \(S \subseteq \beta\) be non-empty.
  Suppose \(x \in S\).
  Then \(x \in \beta\) and transitivity of \(\alpha\) gives \(x \in \alpha\).
  Hence, \(S \subseteq \alpha\).
  Since \(\alpha\) is well-ordered \(S\) has a least element.
  By proposition \ref{prop:ord-well-ordered} \(\beta\) is well-ordered and the
  proof is complete.
\end{proof}

Since \(\in\) is a strict total order in any ordinal it is reasonable to
introduce a more comfortable notation.
\begin{definition}
  If \(\alpha\) and \(\beta\) are ordinals, then one writes \(\alpha < \beta\)
  iff \(\alpha \in \beta\).
\end{definition}
Note that \(<\) is not a relation since there is no set containing all ordinals
and thus \(<\) is not a subset of a Cartesian product.
That there is no set of all ordinals will soon be shown.

There are set theories in which one can quantify over classes.
In such a theory the ordinals constitute a proper class on which one can
rigorously define relations analogously to the definition on sets.
In fact, on the class of ordinals \(<\) will be a strict total order by what
shall soon be done.

On a last note, one can conclude that this detail is a minor practical obstacle.
Whenever one considers a restricted set of ordinals, say \(A\), one can consider
the universe
\[ U = \left(\bigcup_{\alpha \in A} \alpha\right)+ 1. \]
\label{page:odn-union-promise}
As shall be shown, \(U\) is an ordinal (and hence a set) and all ordinals in
\(A\) are contained in \(U\).
On \(U\), \(<\) is an order in the sense defined here.

\begin{definition}
  An initial segment of an ordinal \(\alpha\) is set such that
  \begin{romanenum}
    \item \(A \subseteq \alpha\),
    \item \(x < y \impliess x \in A\) whenever \(y \in A\) and \(x \in \alpha\).
  \end{romanenum}
\end{definition}
\begin{remark}
  The second condition is the definition of being downward closed.
\end{remark}

\begin{lemma}
  If \(\alpha\) is an ordinal and \(A\) is an initial segment of \(\alpha\),
  then \(A\) is an ordinal and \(A \in \alpha\) or \(A = \alpha\).
\end{lemma}
\begin{proof}
  Let \(y \in A\) and \(x \in y\).
  Since \(A \subseteq \alpha\) one has \(y \in \alpha\).
  Transitivity gives \(x \in \alpha\).
  Since also \(x < y\) one has \(x \in A\) by definition showing that \(A\) is
  transitive.
  \(A\) is a subset of a well-ordered set it is itself well-ordered.
  Hence, \(A\) is an ordinal.

  If \(A = \alpha\) there is nothing to show.
  Otherwise \(A \subset \alpha\) and the set
  \[ S = \alpha \setminus A = \{x \in \alpha \ssep x \notin A\} \]
  is non-empty.
  Since \(\alpha\) is well-ordered \(S\) has a least element, say \(\beta\).
  First let \(x \in A\).
  By totality the order \(x \in \beta\), \(x = \beta\) or \(\beta \in x\).
  The two last are impossible since they imply \(\beta \in A\).
  Thus, \(x \in \beta\) and it has been shown that \(A \subseteq \beta\).
  Assume toward a contradiction that \(A \subset \beta\).
  Then there is a \(x \in \beta\) such that \(x \notin A\).
  It follows that \(x \in \alpha\), but this is impossible, because \(x \in S\)
  follows and \(x\) is a smaller element than \(\beta\) contradicting that
  \(\beta\) is the least element of \(S\).
  Hence, \(A = \beta\) and the result follows.
\end{proof}

\begin{proposition}\label{prop:odn-intersection}
  If \(A\) is non-empty set of ordinals, then
  \(\beta = \bigcap_{\alpha \in A} \alpha\) is an ordinal and
  \(\beta \in \alpha\) or \(\beta = \alpha\) for every \(\alpha \in A\).
  Furthermore \(\beta \in A\).
\end{proposition}
\begin{proof}
  Let \(\beta = \bigcap_{\alpha \in A} \alpha\).
  Now \(\beta \subseteq \alpha\) for all \(\alpha \in A\).
  Suppose \(y \in \beta\) and \(x < y\).
  Then \(y \in \alpha\) for all \(\alpha \in A\).
  By transitivity \(x \in \alpha\) for all \(\alpha \in A\).
  Thus, \(x \in \beta\).
  This shows that \(\beta\) is an initial segment of every \(\alpha \in A\) and
  the lemma gives the first part of the conclusion.
  Note that \(\beta \in \alpha\) for all \(\alpha \in A\) implies that
  \(\beta \in \beta\).
  Hence, \(\beta = \alpha\) for some \(\alpha\) and the last assertion follows.
\end{proof}

\begin{corollary}\label{cor:odn-trichotomy}
  If \(\alpha\) and \(\beta\) are ordinals, then precisely one of
  \(\alpha < \beta\), \(\alpha = \beta\) and \(\beta < \alpha\) holds.
\end{corollary}
\begin{proof}
  Let \(A = \alpha \cap \beta\).
  Then
  \begin{romanenum}
    \item \(A \in \alpha\) or \(A = \alpha\),
    \item \(A \in \beta\) or \(A = \beta\),
    \item \(A = \alpha\) or \(A = \beta\).
  \end{romanenum}
  From this the result follows.
\end{proof}

This shows that \(<\) behaves like a connex relation.
Clearly \(<\) also behaves like a irreflexive relation, since
\(\alpha \in \alpha\) is a contradiction.
That \(<\) also exhibits transitivity follows from the fact that only ordinals
are in question.
Thus, \(<\) behaves precisely like a strict total order.

Although \(<\) is not a strict total order one can define \(\le\) as usual by
\[ \alpha \le \beta \iffs \alpha < \beta \vee \alpha = \beta. \]
This pseudo-order will have the ordinary properties of a total order.

\begin{proposition}
  If \(\alpha\) and \(\beta\) are ordinals, then \(\alpha \le \beta\) iff
  \(\alpha \subseteq \beta\).
\end{proposition}
\begin{proof}
  Assume \(\alpha \le \beta\).
  By definition \(\alpha < \beta\) or \(\alpha = \beta\).
  In the latter case it follow directly that \(\alpha \subseteq \beta\).
  In the former case let \(x \in \alpha\).
  By transitivity \(x \in \beta\).
  Because \(x\) is arbitrary one has \(\alpha \subseteq \beta\).

  Au contraire, let \(\alpha \subseteq \beta\).
  If \(\alpha = \beta\) there is nothing to prove.
  Otherwise \(\alpha \ne \beta\).
  Thus, \(\alpha \in \beta\) or \(\beta \in \alpha\).
  The latter yields the contradiction \(\beta \in \beta\), so
  \(\alpha < \beta\).
  In conclusion \(\alpha \le \beta\).
\end{proof}

Furthermore, the ordinals are well-ordered in some sense.
This is the following proposition which is an immediate consequence of
proposition \ref{prop:odn-intersection}.

\begin{proposition}\label{prop:odn-well-ordered}
  Let \(A\) be a non-empty set of ordinals.
  Then \(A\) has a least element.
\end{proposition}

Proposition \ref{prop:odn-well-ordered} is the last proposition needed to show
that there is no set of all ordinals.
Suppose there is such a set, say \(A\).
Let \(y \in A\) and \(x \in y\).
By definition of \(A\), \(y\) is an ordinal and hence also \(x\) is an ordinal
and the definition again gives \(x \in A\) showing that \(A\) is transitive.
If \(x,y \in A\), then by corollary \ref{cor:odn-trichotomy} one has requirement
(i) of proposition \ref{prop:ord-well-ordered}.
That requirement (ii) is also fulfilled follows from proposition
\ref{prop:odn-well-ordered}, since if \(S \subseteq A\) then \(S\) is a set of
ordinals, so it has a least element.
Thus, \(A\) is well-ordered, showing that \(A\) is an ordinal.
This leads to the contradiction \(A \in A\).

\begin{lemma}
  If \(\alpha\) and \(\beta\) are ordinals, then \(\alpha \cup \beta\) is an
  ordinal.
\end{lemma}
\begin{proof}
  Either \(\alpha < \beta\), \(\alpha = \beta\) or \(\beta < \alpha\).
  If \(\alpha = \beta\) the result is clear.
  Otherwise it can without loss of generality be assumed that
  \(\alpha < \beta\).
  By transitivity \(\alpha \subset \beta\).
  Thus, \(\alpha \cup \beta = \beta\) and the proof is complete.
\end{proof}

The proposition can be generalized.
This fulfills the promise made on page \pageref{page:odn-union-promise}.
\begin{proposition}
  If \(A\) is a set of ordinals, then \(\bigcup_{\alpha \in A} \alpha\) is an
  ordinal.
\end{proposition}
\begin{proof}
  Let \(\beta = \bigcup_{\alpha \in A} \alpha\).
  First transitivity.
  Suppose \(y \in \beta\) and \(x \in y\).
  By definition \(y \in \alpha\) for some \(\alpha \in A\).
  Since \(\alpha\) is an ordinal one has \(x \in \alpha\).
  Hence, \(x \in \beta\).

  Next it must be shown that \(\beta\) is well-ordered.
  This is done as usual through proposition \ref{prop:ord-well-ordered}.
  Suppose \(x,y \in \beta\).
  By definition \(x \in \alpha_1\) and \(y \in \alpha_2\) for some
  \(\alpha_1,\alpha_2 \in A\).
  One has \(x,y \in \alpha_1 \cup \alpha_2\) and since
  \(\alpha_1 \cup \alpha_2\) is an ordinal one has precisely one of \(x \in y\),
  \(x = y\) and \(y \in x\).

  Let \(S \subseteq \beta\) be non-empty.
  Note that \(x \in S\) implies \(x \in \alpha\) for some \(\alpha \in A\).
  In particular \(x\) is an ordinal.
  Therefore \(S\) is a set of ordinals.
  By a previous proposition \(S\) has a least element.
\end{proof}

The work so far has just been formal, since no assertions about existence of
ordinals have been made, except for the empty set.
It follows by induction that there are countably many finite ordinals.
That there is an infinite ordinal follows from the infinity axiom, much in the
same way the natural numbers were constructed.
In fact, with the definition of \(\setN\) as was given in this document one has
that \(\setN\) is an ordinal, as is easily verified.
In set theory one calls the first infinite ordinal (i.e. \(\setN\)) \(\omega\).

\begin{definition}
  An ordinal \(\alpha\) is said to be a limit ordinal if there does not exist an
  ordinal \(\beta\) such that \(\alpha = \beta + 1\).
\end{definition}

Note that \(0\) is by this definition a limit ordinal.
The main result of this section is the possibility of doing transfinite
induction.
In a sense it works much like induction with the natural numbers, but it is much
stronger since it can prove that something holds for all ordinals, and not just
all natural numbers.

\begin{theorem}[Transfinite induction]
  Let \(\phi(\alpha)\) be a logical formula with \(\alpha\) as its only free
  variable.
  Suppose the following conditions are satisfied.
  \begin{romanenum}
    \item \(\phi(0)\) is true.
    \item \(\phi(\alpha) \impliess \phi(\alpha+1)\).
    \item
      For all limit ordinals \(\lambda\), \(\phi(\beta)\) for all
      \(\beta < \lambda\) implies \(\phi(\lambda)\).
  \end{romanenum}
  Then \(\phi(\alpha)\) is true for all ordinals \(\alpha\).
\end{theorem}
\begin{proof}
  Suppose towards a contradiction that \(\neg\phi(\gamma)\) for some ordinal
  \(\gamma\).
  Let
  \[ S = \{\alpha \subseteq \gamma \ssep \neg\phi(\alpha)\}. \]
  Now \(S\) is non-empty so it has a least element, say \(\beta\).
  First suppose \(\beta\) is a successor ordinal.
  Then \(\beta = \delta + 1\) for some ordinal \(\delta\).
  Now \(\delta < \beta\) so \(\delta \notin S\).
  This means \(\phi(\delta)\) and (ii) gives \(\phi(\delta+1)\) which
  contradicts that \(\beta \in S\).
  Therefore \(\beta\) is not a successor, so it is a limit ordinal.
  \(\beta \ne 0\) by (i).
  For all \(\delta < \beta\) one has \(\phi(\delta)\) since \(\beta\) is the
  least element of \(S\).
  Thus, (iii) gives \(\phi(\beta)\) which is again a contradiction.
  Therefore \(\phi(\alpha)\) for all ordinals \(\alpha\).
\end{proof}

From all the theory in this chapter one can develop ordinal arithmetic, which
generalizes arithmetic in \(\setN\).
The possibility to do arithmetic motivates the use of language ``ordinal
number''.
However, this will not be done since transfinite induction is the main result
needed in the following material.

The next lemma is placed here temporarily. % FIXME
\begin{lemma}
  Given ordinals \(\alpha\) and \(\beta\), \(\alpha \cong \beta\) iff
  \(\alpha = \beta\).
\end{lemma}
\begin{proof}
  Only the non-trivial implication is shown.
  Suppose there are ordinals such that \(\alpha \cong \beta\) and
  \(\alpha \ne \beta\).
  Without loss of generality it can be assumed that \(\alpha < \beta\).
  Let \(\alpha\) be the least ordinal with this property and let
  \(\varphi: \beta \to \alpha\) be an isomorphism.
  One has \(\alpha \subset \beta\).
  Therefore there is a \(\delta \in \beta\) such that \(\delta \notin \alpha\).
  By construction \(\varphi[\alpha] \subseteq \alpha\).
  Since \(\alpha\) is an initial segment of \(\beta\), \(\varphi[\alpha]\) is an
  initial segment of \(\alpha\).
  Thus, \(\varphi[\alpha]\) is an ordinal, say \(\gamma\).
  Now \(\gamma = \alpha\) or \(\gamma \in \alpha\).
  One cannot have \(\gamma = \alpha\), since by injectivity
  \(\varphi(\delta) \notin \gamma\) but \(\varphi(\delta) \in \alpha\).
  Hence, \(\gamma \in \alpha\).
  Now the restriction
  \[ \restr{\varphi}{\alpha}: \alpha \to \gamma \]
  is an isomorphism so that \(\gamma \cong \alpha\) but \(\gamma < \alpha\)
  contradicting minimality of \(\alpha\).
  Therefore there are no ordinals with \(\alpha \cong \beta\) and
  \(\alpha \ne \beta\).
\end{proof}

% FIXME: every well-ordered set is isomorphic to a unique ordinal
