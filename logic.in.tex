\section{Logic}

In this section the logical framework will be introduced, which will be used
throughout the entire text.
The first study is propositional logic.

\subsection{Propositional logic}

Propositional logic deals with formulas, to which one can assign truth values,
which will here be restricted to ``true'' and ``false''.
In order to have something to work with one needs to introduce the concept of
a atomic formula.

\begin{definition}
  The truth values are ``True'' and ``False''.
\end{definition}

\begin{definition}
  A logical system contains atomic formulas which have been assigned truth
  values.
  An assignment of truth values to the atomic formulas is called a valuation.
\end{definition}
For short atomic formulas will be called ``atoms'' in this section.
In this section the atoms will be denoted by the letters
\[ p, q, r, s, t. \]
In spite of the limited number of symbols there can be any number of atomic
formulas in a logical system.

From the atoms one can construct (compound) formulas by joining them using
connectives, which initially will be the following ones:
\begin{romanenum}
  \item
  conjunction, \(\wedge\),
  \item
  disjunction, \(\vee\),
  \item
  negation, \(\neg\).
\end{romanenum}

It shall soon be defined what these symbols should mean.

\begin{definition}
  The atomic formulas are well-formed formulas (WFF).
  If \(\alpha\) and \(\beta\) are WFFs then the following formulas are WFFs:
  \[
    (\alpha \vee \beta),
    \quad
    (\alpha \wedge \beta)
    \quad \text{and} \quad
    (\neg \alpha).
  \]
  There are no other WFFs other than those which arise in this way.
\end{definition}

As the reader have seen \(\alpha\) and \(\beta\) are used as symbols for WFFs.
Also \(\gamma\) will be used.
As in the previous section parentheses will be left out when there is no
syntactical ambiguity.

Now that the formality is in place one can state the basic axioms of logic.

\begin{definition}
  A WFF which has been assigned the truth value True is said to be true.
  False WFFs are defined analogously.
\end{definition}

\begin{named}[Axiom of existence]
  There is a true WFF, which is denoted by \(\true\).
  There is a false WFF, which is denoted by \(\false\).
\end{named}

Usually this notation will be abused, so T is used both for the truth value
True and for a formula which is true.
Likewise for F.

So far only the atoms have been assigned truth values.
The following definitions describe how all sentences are assigned truth values.
\begin{definition}
  Let \(\alpha\) and \(\beta\) be valuated well-formed formulas.
  Then the WFFs \(\alpha \vee \beta\) and \(\alpha \wedge \beta\) are assigned
  truth values according to the following truth table.
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\alpha \vee \beta\) & \(\alpha \wedge \beta\) \\
      \hline
      F          & F         & F                     & F                       \\
      F          & T         & T                     & F                       \\
      T          & F         & T                     & F                       \\
      T          & T         & T                     & T
    \end{tabular}
  \end{table}
\end{definition}

The disjunction \(\vee\) should of course mean what is usually called
``inclusive or''.
Likewise the conjunction \(\wedge\) means the ordinary ``and''.

\begin{definition}
  Let \(\alpha\) be a valuated well-formed formula.
  Then the WFF \(\neg \alpha\) is assigned a truth value according to the
  following truth table.
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c}
      \(\alpha\) & \(\neg \alpha\) \\
      \hline
      F          & T               \\
      T          & F
    \end{tabular}
  \end{table}
\end{definition}

The reader may wonder if the definitions are good.
What prevents a WFF from being both true and false?
The answer is the following principle, which is taken as an axiom.

\begin{named}[Principle of bivalence]
  Every WFF is either true or false.
\end{named}

Using mathematics one can show that the assignment of truth values is good, i.e.
when the truth values of the atomic formulas are know every WFF can be assigned
a unique truth value.
Hence, one could have settled with the following weaker axiom.
However, since no mathematics has been introduced yet the stronger axiom will be
used.
\begin{named}[Weak principle of bivalence]
  Every atomic formula is either true or false.
\end{named}

\begin{definition}
  A WFF which is true regardless of valuation is said to be a tautology.
  A WFF which is false under the same conditions is called a contradiction.
\end{definition}

When stating logical theorems, one states tautologies.
For example the following laws are logical theorems, which can be derived from
the principle of bivalence.
\begin{named}[Law of non-contradiction]
  \(\neg (\alpha \wedge \neg \alpha)\).
\end{named}
The law states that a formula and its negation cannot be true simultaneously,
which is clear from the following truth table
\begin{table}[H]
  \centering
  \begin{tabular}{c|c|c|c}
    \(\alpha\) & \(\neg \alpha\) & \(\alpha \wedge \neg \alpha\) & \(\neg (\alpha \wedge \neg \alpha)\) \\
    \hline
    F          & T               & F                             & T                                    \\
    T          & F               & F                             & T
  \end{tabular}
\end{table}

\begin{named}[Law of excluded middle]
  \(\alpha \vee \neg \alpha\).
\end{named}
The second one states that a formula is true or its negation is, which is
evident from the following table.
\begin{table}[H]
  \centering
  \begin{tabular}{c|c|c}
    \(\alpha\) & \(\neg \alpha\) & \(\alpha \vee \neg \alpha\) \\
    \hline
    F          & T               & T                           \\
    T          & F               & T
  \end{tabular}
\end{table}

\begin{definition}[Implication]
  The connective \(\impliess\) is defined so that \(\alpha \impliess \beta\) is
  the same as \(\beta \vee \neg \alpha\).
\end{definition}

By constructing the table for the implication it is clear what it should mean.
\begin{table}[H]
  \centering
  \begin{tabular}{c|c|c|c}
    \(\alpha\) & \(\beta\) & \(\neg \alpha\) & \(\alpha \impliess \beta\) \\
    \hline
    F          & F         & T               & T                          \\
    F          & T         & T               & T                          \\
    T          & F         & F               & F                          \\
    T          & T         & F               & T
  \end{tabular}
\end{table}
One wants the implication to mean ``if \(\alpha\), then \(\beta\)'', which is
indeed the case as seen in the table.
The crucial property of the implication is its transitivity which is the content
of the following theorem.
\begin{theorem}
  \(
    ((\alpha \impliess \beta) \wedge (\beta \impliess \gamma))
    \impliess (\alpha \impliess \gamma)
  \).
\end{theorem}
\begin{proof}
  As expected the proof is a truth table showing that the sentence is a
  tautalogy.
  For the table to be readable the following synonyms are introduced
  \[
    A: \alpha \impliess \beta,
    \quad
    B: \beta \impliess \gamma,
    \quad
    C: \alpha \impliess \gamma.
  \]
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\gamma\) & \(A\) & \(B\) & \(C\) & \((A \wedge B)\) & \((A \wedge B) \impliess C\) \\
      \hline
      F          & F         & F          & T     & T     & T     & T                & T                            \\
      F          & F         & T          & T     & T     & T     & T                & T                            \\
      F          & T         & F          & T     & F     & T     & F                & T                            \\
      F          & T         & T          & T     & T     & T     & T                & T                            \\
      T          & F         & F          & F     & T     & F     & F                & T                            \\
      T          & F         & T          & F     & T     & T     & F                & T                            \\
      T          & T         & F          & T     & F     & F     & F                & T                            \\
      T          & T         & T          & T     & T     & T     & T                & T
    \end{tabular}
  \end{table}
\end{proof}

With the implication it is trivial to construct the equivalence and consequently
its corresponding truth table.
\begin{definition}[Equivalence]
  The connective \(\iffs\) is defined so that \(\alpha \iffs \beta\) is the same
  as \((\alpha \impliess \beta) \wedge (\beta \impliess \alpha)\).
\end{definition}
\begin{table}[H]
  \centering
  \begin{tabular}{c|c|c|c|c}
    \(\alpha\) & \(\beta\) & \(\alpha \impliess \beta\) & \(\beta \impliess \alpha\) & \(\alpha \iffs \beta\) \\
    \hline
    F          & F         & T                          & T                          & T                      \\
    F          & T         & T                          & F                          & F                      \\
    T          & F         & F                          & T                          & F                      \\
    T          & T         & T                          & T                          & T
  \end{tabular}
\end{table}
It is readily seen that the equivalence satisfies the desired property, that is
``\(\alpha \iffs \beta\)'' means the same as ``\(\alpha\) if and only if
\(\beta\)''.
A direct consequence is \(\alpha \iffs \alpha\).
Also, by the symmetry in the table one sees that \(\alpha \iffs \beta\) is the
same as \(\beta \iffs \alpha\), i.e.
\[ (\alpha \iffs \beta) \iffs (\beta \iffs \alpha). \]
One can also show that the equivalence is transitive.
\begin{theorem}
  \(
    ((\alpha \iffs \beta) \wedge (\beta \iffs \gamma))
    \impliess (\alpha \iffs \gamma)
  \).
\end{theorem}
\begin{proof}
  Again the proof is a truth table showing that the sentence is a tautology.
  For the table to be readable the following synonyms are introduced
  \[
    A: \alpha \iffs \beta,
    \quad
    B: \beta \iffs \gamma,
    \quad
    C: \alpha \iffs \gamma.
  \]
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\gamma\) & \(A\) & \(B\) & \(C\) & \((A \wedge B)\) & \((A \wedge B) \impliess C\) \\
      \hline
      F          & F         & F          & T     & T     & T     & T                & T                            \\
      F          & F         & T          & T     & F     & F     & F                & T                            \\
      F          & T         & F          & F     & F     & T     & F                & T                            \\
      F          & T         & T          & F     & T     & F     & F                & T                            \\
      T          & F         & F          & F     & T     & F     & F                & T                            \\
      T          & F         & T          & F     & F     & T     & F                & T                            \\
      T          & T         & F          & T     & F     & F     & F                & T                            \\
      T          & T         & T          & T     & T     & T     & T                & T
    \end{tabular}
  \end{table}
\end{proof}

Note that one could logic without implication and equivalence, since they are
synonymous to expressions containing only the connectives \(\wedge\), \(\vee\)
and \(\neg\).
However, such an approach is by necessity very tedious.

Consider the following well formed formula
\[ (((\neg p) \wedge q) \impliess r) \iffs (q \vee r), \]
which has a lot of parentheses.
To avoid such hard to read formulas operator precedence is introduced.
\begin{named}[Operator precedence]
  In a well-formed formula the connectives are evaluated in the following order
  from the first to the last
  \[ \neg, \wedge, \vee, \impliess, \iffs. \]
\end{named}
Using operator precedence the above formula becomes
\[ \neg p \wedge q \impliess r \iffs q \vee r. \]

\begin{named}[Basic rules of logic]
  Propositional logic adheres the following rules.
  \begin{enumerate}[twocol]
    \item
    \(\alpha \vee \beta \iffs \beta \vee \alpha\).
    \item
    \(\alpha \vee (\beta \vee \gamma) \iffs (\alpha \vee \beta) \vee \gamma\).
    \item
    \(
      \alpha \vee (\beta \wedge \gamma)
      \iffs
      (\alpha \vee \beta) \wedge (\alpha \vee \gamma)\).
    \item
    \(\alpha \vee \false \iffs \alpha\).
    \item
    \(\alpha \vee \neg \alpha \iffs \true\).
    \item
    \(\alpha \wedge \beta \iffs \beta \wedge \alpha\).
    \item
    \(
      \alpha \wedge (\beta \wedge \gamma)
      \iffs
      (\alpha \wedge \beta) \wedge \gamma\).
    \item
    \(
      \alpha \wedge (\beta \vee \gamma)
      \iffs
      (\alpha \wedge \beta) \vee (\alpha \wedge \gamma)\).
    \item
    \(\alpha \wedge \true \iffs \alpha\).
    \item
    \(\alpha \wedge \neg \alpha \iffs \false\).
  \end{enumerate}
\end{named}
\begin{proof}
  The proofs are carried out using truth tables in which it is shown that the
  left hand side of the equivalence always has the same truth value as the right
  hand side.

  The following table proves 1 and 6.
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\alpha \vee \beta\) & \(\beta \vee \alpha\) & \(\alpha \wedge \beta\) & \(\beta \wedge \alpha\) \\
      \hline
      F          & F         & F                     & F                     & F                       & F                       \\
      F          & T         & T                     & T                     & F                       & F                       \\
      T          & F         & T                     & T                     & F                       & F                       \\
      T          & T         & T                     & T                     & T                       & T
    \end{tabular}
  \end{table}
  For 2 consider the following table.
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\gamma\) & \(\alpha \vee \beta\) & \(\beta \vee \gamma\) & \(\alpha \vee (\beta \vee \gamma)\) & \((\alpha \vee \beta) \vee \gamma\) \\
      \hline
      F          & F         & F          & F                     & F                     & F                                   & F                                   \\
      F          & F         & T          & F                     & T                     & T                                   & T                                   \\
      F          & T         & F          & T                     & T                     & T                                   & T                                   \\
      F          & T         & T          & T                     & T                     & T                                   & T                                   \\
      T          & F         & F          & T                     & F                     & T                                   & T                                   \\
      T          & F         & T          & T                     & T                     & T                                   & T                                   \\
      T          & T         & F          & T                     & T                     & T                                   & T                                   \\
      T          & T         & T          & T                     & T                     & T                                   & T
    \end{tabular}
  \end{table}
  The similar table for 7 follows.
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\gamma\) & \(\alpha \wedge \beta\) & \(\beta \wedge \gamma\) & \(\alpha \wedge (\beta \wedge \gamma)\) & \((\alpha \wedge \beta) \wedge \gamma\) \\
      \hline
      F          & F         & F          & F                       & F                       & F                                       & F                                       \\
      F          & F         & T          & F                       & F                       & F                                       & F                                       \\
      F          & T         & F          & F                       & F                       & F                                       & F                                       \\
      F          & T         & T          & F                       & T                       & F                                       & F                                       \\
      T          & F         & F          & F                       & F                       & F                                       & F                                       \\
      T          & F         & T          & F                       & F                       & F                                       & F                                       \\
      T          & T         & F          & T                       & F                       & F                                       & F                                       \\
      T          & T         & T          & T                       & T                       & T                                       & T
    \end{tabular}
  \end{table}
  The tables for 3 and 8 are:
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\gamma\) & \(\beta \wedge \gamma\) & \(\alpha \vee (\beta \wedge \gamma)\) & \(\alpha \vee \beta\) & \(\alpha \vee \gamma\) & \((\alpha \vee \beta) \wedge (\alpha \vee \gamma)\) \\
      \hline
      F          & F         & F          & F                       & F                                     & F                     & F                      & F                                                   \\
      F          & F         & T          & F                       & F                                     & F                     & T                      & F                                                   \\
      F          & T         & F          & F                       & F                                     & T                     & F                      & F                                                   \\
      F          & T         & T          & T                       & T                                     & T                     & T                      & T                                                   \\
      T          & F         & F          & F                       & T                                     & T                     & T                      & T                                                   \\
      T          & F         & T          & F                       & T                                     & T                     & T                      & T                                                   \\
      T          & T         & F          & F                       & T                                     & T                     & T                      & T                                                   \\
      T          & T         & T          & T                       & T                                     & T                     & T                      & T                                                   \\
    \end{tabular}
  \end{table}
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c|c}
      \(\alpha\) & \(\beta\) & \(\gamma\) & \(\beta \vee \gamma\) & \(\alpha \wedge (\beta \vee \gamma)\) & \(\alpha \wedge \beta\) & \(\alpha \wedge \gamma\) & \((\alpha \wedge \beta) \vee (\alpha \wedge \gamma)\) \\
      \hline
      F          & F         & F          & F                     & F                                     & F                       & F                        & F                                                     \\
      F          & F         & T          & F                     & F                                     & F                       & F                        & F                                                     \\
      F          & T         & F          & F                     & F                                     & F                       & F                        & F                                                     \\
      F          & T         & T          & F                     & F                                     & F                       & F                        & F                                                     \\
      T          & F         & F          & F                     & F                                     & F                       & F                        & F                                                     \\
      T          & F         & T          & T                     & T                                     & F                       & T                        & T                                                     \\
      T          & T         & F          & T                     & T                                     & T                       & F                        & T                                                     \\
      T          & T         & T          & T                     & T                                     & T                       & T                        & T                                                     \\
    \end{tabular}
  \end{table}
  4 and 9 follow from the following simple table, where it is used that F only
  can attain the truth value False and likewise for T.
  \begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c}
      \(\alpha\) & F & T & \(\alpha \vee \false\) & \(\alpha \wedge \true\) \\
      \hline
      F          & F & T & F                      & F                       \\
      T          & F & T & T                      & T                       \\
    \end{tabular}
  \end{table}
  Lastly, 5 and 10 are the law of excluded middle and the law of
  non-contradiction, respectively.
\end{proof}

Finally it can be concluded that propositional logic forms a symbolic boolean
algebra, where the equality is represented by \(\iffs\), \(0\) by F and \(1\) by
T.
Therefore all results that were deduced symbolically before acquire a meaning in
terms of logic.
For example one can deduce
\[
  \neg\neg\alpha \iffs \alpha,
  \quad
  \alpha \vee \true \iffs \true,
  \quad
  \alpha \wedge \alpha \iffs \alpha
  \quad \text{and} \quad
  \neg(\alpha \wedge \beta) \iffs \neg\alpha \vee \neg\beta.
\]
simply by applying this realization.

In the upcoming mathematics a few results will be necessary.
The next theorem is the formalized version of the phrase ``in particular''.
\begin{theorem}
  \(\alpha \wedge \beta \impliess \alpha\).
\end{theorem}
\begin{proof}
  \[
    \begin{split}
      \alpha \wedge \beta \impliess \alpha
      &\iffs
      \neg (\alpha \wedge \beta) \vee \alpha \\
      &\iffs
      \neg \alpha \vee \neg \beta \vee \alpha \\
      &\iffs
      (\alpha \vee \neg \alpha) \vee \neg \beta \\
      &\iffs
      \true \vee \neg \beta \\
      &\iffs
      \true.
      \qedhere
    \end{split}
  \]
\end{proof}

\begin{theorem}
  \(
    (\alpha \impliess \beta) \wedge (\alpha \impliess \gamma)
    \iffs
    (\alpha \impliess \beta \wedge \gamma).
  \)
\end{theorem}
\begin{proof}
  \[
    \begin{split}
      (\alpha \impliess \beta) \wedge (\alpha \impliess \gamma)
      &\defiffs
      (\neg \alpha \vee \beta) \wedge (\neg \alpha \vee \gamma) \\
      &\iffs
      \neg \alpha \vee (\beta \wedge \gamma) \\
      &\defiffs
      \alpha \impliess \beta \wedge \gamma.
      \qedhere
    \end{split}
  \]
\end{proof}
\begin{corollary}
  \(\alpha \impliess \beta \iffs \alpha \impliess \alpha \wedge \beta\).
\end{corollary}
\begin{proof}
  Note that \(\alpha \impliess \alpha\) is a tautology.
  Therefore
  \[
    \alpha \impliess \beta
    \iffs
    (\alpha \impliess \alpha) \wedge (\alpha \impliess \beta)
    \iffs
    \alpha \impliess (\alpha \wedge \beta).
    \qedhere
  \]
\end{proof}
\begin{corollary}
  \((\alpha \impliess \beta \wedge \gamma) \impliess (\alpha \impliess \beta)\).
\end{corollary}

When working a logical system one must have some way to infer new propositions
from proved ones.
To do this one uses \emph{rules of inference}.
The first that will be used is modus ponens, which can be written using an
inference table like the one below.
\begin{table}[H]
  \centering
  \begin{tabular}{l}
    \(\alpha\) \\
    \(\alpha \impliess \beta\) \\
    \hline
    \(\therefore \beta\)
  \end{tabular}
\end{table}
Above the horizontal line are the premises and below is the conclusion.
The symbol \(\therefore\) is read as ``therefore''.
Modus ponens says that whenever \(\alpha\) is true and
\(\alpha \impliess \beta\), then \(\beta\).
In fact this rule can be proved, by showing
\[ (\alpha \wedge (\alpha \impliess \beta)) \impliess \beta. \]
By using the definition of \(\impliess\) and De Morgan's law one has
\[
  \begin{split}
    (\alpha \wedge (\alpha \impliess \beta)) \impliess \beta
      &\iffs \beta \vee \neg(\alpha \wedge (\beta \vee \neg \alpha) \\
      &\iffs \beta \vee (\neg\alpha \vee \neg(\beta \vee \neg \alpha)) \\
      &\iffs (\beta \vee \neg\alpha) \vee \neg(\beta \vee \neg \alpha) \\
      &\iffs \true.
  \end{split}
\]
From modus ponens, one can derive modus tollens.
Before this is possible a theorem is required.

\begin{theorem}
  \((\alpha \impliess \beta) \iffs (\neg\beta \impliess \neg \alpha)\).
\end{theorem}
\begin{proof}
  By using the definition of \(\impliess\) and some algebra one has
  \[
    (\alpha \impliess \beta)
    \iffs
    (\beta \vee \neg\alpha)
    \iffs
    (\neg(\neg\beta) \vee \neg\alpha)
    \iffs
    (\neg\alpha \vee \neg(\neg\beta))
    \iffs
    (\neg\beta \impliess \neg\alpha).
    \qedhere
  \]
\end{proof}

\indent By modus ponens one has
\begin{table}[H]
  \centering
  \begin{tabular}{l}
    \(\neg \beta\) \\
    \(\neg \beta \impliess \neg \alpha\) \\
    \hline
    \(\therefore \neg \alpha\)
  \end{tabular}
\end{table}
\noindent but according to the proposition this is the same as
\begin{table}[H]
  \centering
  \begin{tabular}{l}
    \(\neg \beta\) \\
    \(\alpha \impliess \beta\) \\
    \hline
    \(\therefore \neg \alpha\)
  \end{tabular}
\end{table}
\noindent which is precisely modus tollens.
This is the basis for contradiction proofs where one assumes \(\alpha\) to show
the implication \(\alpha \impliess \beta\), where \(\beta\) is a contradiction,
i.e. \(\neg \beta\).
Now by modus tollens \(\neg \alpha\), so the assumption \(\alpha\) is false.

\subsection{Predicate logic}

\begin{definition}[Equality]
  Let there be given any \(x\) and \(y\).
  \(x\) and \(y\) are said to be equal if \(Px \iffs Py\) for all
  predictates \(P\).
  This is written as \(x = y\).
\end{definition}
% FIXME: prove reflexivity, symmetry and transitivity
% FIXME: define \neq

\begin{proposition}
  \(\exists x (Px \vee Qx) \iffs \exists x(Px) \vee \exists x(Px)\).
\end{proposition}

% FIXME: predicate logic
% FIXME: define free and bound variables

% FIXME: (\forall x \alpha) \vee \beta \iffs \forall x(\alpha \vee \beta)
