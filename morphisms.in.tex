\section{Morphisms}

In the section on natural numbers \(n\)-tuples and \(n\)-fold Cartesian products
were defined.
These concepts will now be used to define \(n\)-ary relations which will be
necessary to define morphisms generally.

\begin{definition}
  An \(n\)-ary relation \(R\) on a set \(S\) is a subset of \(S^n\).
\end{definition}

Given a function \(\varphi: S \to T\) it will in this section be necessary to
apply it to each component to a Cartesian product so as to create a function
\(S^n \to T^n\).
In this section this function will be denoted by \(\varphi_n\) and is defined as
\[ \varphi_n: S^n \ni (x_k)_1^n \mapsto (\varphi(x_k))_1^n \in T^n, \]
which is just a compact way of writing
\[ \varphi_n(x_1,\dots,x_n) = (\varphi(x_1),\dots,\varphi(x_n)). \]

\begin{lemma}\label{lem:isom-bijection}
  If \(\varphi\) is a bijection, then \(\varphi_n\) is a bijection with inverse
  function \(\inv{(\varphi_n)} = (\inv{\varphi})_n\).
\end{lemma}
\begin{proof}
  If the inverse relation \(\inv{(\varphi_n)}\) exists and is a function, then
  \(\varphi_n\) is bijective.
  It therefore suffices to show that \((\inv{\varphi})_n\) is a function and
  indeed is the inverse of \(\varphi_n\).
  By assumption \(\inv{\varphi}\) exists and so \((\inv{\varphi})_n\) is a
  well-defined function.
  Take any \((x_k)_1^n \in S^n\).
  Then
  \[
    \begin{split}
      ((\inv{\varphi})_n \circ \varphi_n)((x_k)_1^n)
        &= (\inv{\varphi})_n(\varphi_n((x_k)_1^n)) \\
        &= (\inv{\varphi})_n((\varphi(x_k))_1^n) \\
        &= (\inv{\varphi}(\varphi(x_k)))_1^n \\
        &= ((\inv{\varphi} \circ \varphi)(x_k))_1^n \\
        &= (x_k)_1^n.
    \end{split}
  \]
  Since \((x_k)_1^n\) is arbitrary this shows that
  \[ (\inv{\varphi})_n \circ \varphi_n = I_{S^n}. \]
  That also
  \[ \varphi_n \circ (\inv{\varphi})_n = I_{S^n} \]
  is shown similarly.
\end{proof}

The implication in the lemma is reversible.

\begin{lemma}
  If \(\varphi_n\) is a bijection, then \(\varphi\) is a bijection.
\end{lemma}
\begin{proof}
  Suppose \(\varphi_n\) is a bijection.
  Assume \(\varphi(s) = \varphi(s')\).
  Then indeed \(\varphi_n((s,s,\dots,s)) = \varphi_n((s',s',\dots,s'))\), so
  that \((s,s,\dots,s) = (s',s',\dots,s')\) follows by the injectivity of
  \(\varphi_n\).
  Therefore \(s = s'\) and \(\varphi\) is injective.
  Let \(t \in T\).
  Then \(\t = (t,t,\dots,t) \in T^n\) and so there exists an
  \(\s = (s_1,s_2,\dots,s_n)\) such that \(\varphi_n(\s) = \t\).
  Therefore \(\varphi(s_k) = t\) where \(s_k \in S\).
  Hence, \(\varphi\) is surjective.
\end{proof}

The lemmas make it is possible to write
\[ \inv{\varphi_n} = \inv{(\varphi_n)} = (\inv{\varphi})_n \]
without ambiguity.
Of course this only makes sense when \(\varphi\) is a bijection.

Before introducing the main concept of this subsection another lemma is needed.

\begin{lemma}
  Let \(\varphi: T \to U\) and \(\psi: S \to T\) and set
  \(\gamma = \varphi \circ \psi\).
  Then \(\gamma_n = \varphi_n \circ \psi_n\).
\end{lemma}
\begin{proof}
  Let \(\x = (x_k)_1^n \in S^n\).
  By applying definitions one has
  \[
    \begin{split}
      \gamma_n(\x)
        &= \gamma_n((x_k)_1^n)
         = (\gamma(x_k))_1^n \\
        &= (\varphi(\psi(x_k)))_1^n
        = \varphi_n((\psi(x_k))_1^n) \\
        &= \varphi_n(\psi_n((x_k)_1^n))
        = \varphi_n(\psi_n(\x)).
    \end{split}
  \]
  Since \(\x\) is arbitrary this shows that
  \(\gamma_n = \varphi_n \circ \psi_n\).
\end{proof}

\begin{definition}
  Let \(S\) and \(\overline{S}\) be sets and let \(R_1,\dots,R_m\) and
  \(\overline{R}_1,\dots,\overline{R}_m\) be relations on \(S\) and
  \(\overline{S}\) respectively.
  If \(\varphi: S \to \overline{S}\) is a bijection such that for all \(n\)-ary
  relations \(R_k\) one has
  \[ \x \in R_k \iffs \varphi_n(\x) \in \overline{R}_k, \]
  then \(\varphi\) is said to be an \emph{isomorphism} between \(S\) and
  \(\overline{S}\) with respect to the given relations.
  Also \(S\) and \(\overline{S}\) are said to be isomorphic (with respect to the
  given relations) and one writes \(S \cong \overline{S}\).
\end{definition}

The following proposition is trivial, so the proof is left out.
\begin{proposition}
  The identity function \(I_S\) is an isomorphism from \(S\) to \(S\).
\end{proposition}

\begin{proposition}
  If \(\varphi\) is an isomorphism \(S \to T\), then \(\inv{\varphi}\) is an
  isomorphism \(T \to S\).
\end{proposition}
\begin{proof}
  Clearly \(\inv{\varphi}\) is a bijection \(T \to S\).
  Let \(R_S\) be a respected relation on \(S\) and let \(R_T\) be the
  corresponding relation in \(T\).
  Take \(\y \in R_T\).
  Since \(\varphi_n\) is bijective \(\varphi_n(\x) = \y\) for precisely one
  \(\x\), but then by the defining equivalence one has \(\x \in R_S\), but
  \(\x = \inv{\varphi_n}(\y)\), so
  \[ \y \in R_T \impliess \inv{\varphi_n}(\y) \in R_S. \]
  Next suppose \(\inv{\varphi_n}(\y) \in R_S\).
  Then by the equivalence one has that
  \[ \varphi_n(\inv{\varphi_n}(\y)) = \y \in R_T \]
  which shows that \(\inv{\varphi}\) respects the relations \(R_S\) and \(R_T\).
  Since these are arbitrary \(\inv{\varphi}\) is an isomorphism.
\end{proof}

\begin{proposition}\label{prop:isom-trans}
  If \(\varphi\) is an isomorphism from \(T \to U\) and \(\psi\) is an
  isomorphism \(S \to T\), then \(\gamma = \varphi \circ \psi\) is an
  isomorphism \(S \to U\).
\end{proposition}
\begin{proof}
  Indeed, \(\gamma\) is a bijection \(S \to U\).
  Let \(R_S\) be any \(n\)-ary respected relation on \(S\) and let \(R_T\) and
  \(R_U\) be the corresponding relations on \(T\) and \(U\).
  By definition one has that
  \[
    \x \in R_S \iffs \psi_n(\x) \in R_T
    \quad \text{and} \quad
    \y \in R_T \iffs \varphi_n(\y) \in R_U.
  \]
  By applying both of the equivalences and the lemma above one has
  \[
    \x \in R_S
    \iffs
    \psi_n(\x) \in R_T
    \iffs
    \varphi_n(\psi_n(\x)) \in R_U
    \iffs
    \gamma_n(\x) \in R_U,
  \]
  which means \(\gamma\) respects \(R_S\) and \(R_U\), but since these are
  arbitrary \(\gamma\) respects all relation that \(\varphi\) and \(\psi\)
  respects showing that \(\gamma\) is an isomorphism \(S \to U\).
\end{proof}

By the propositions one has that isomorphism possess following properties.
\begin{romanenum}
  \item
  \(S \cong S\).
  \item
  If \(S \cong T\), then \(T \cong S\).
  \item
  If \(S \cong T\) and \(T \cong U\), then \(S \cong U\).
\end{romanenum}
Hence isomorphism behaves like an equivalence relation.
The second property allows one to speak about isomorphisms between \(S\) and
\(T\) rather that from \(S\) to \(T\) or vice versa.

Algebraically, isomorphic structures are essentially identical and an
isomorphism can be seen as a renaming of the elements and the relations.

The injectivity and the leftwards implication in the isomorphism definition
are indispensable; none of then can be dropped without producing a flawed or
different definition.
If \(\varphi\) would not be required to be injective the definition would be
bad, since one could have \(\varphi_n(\x_1) = \varphi_n(\x_2)\) where
\(\x_1 \in R_k\) and \(\x_2 \notin R_k\) in effect breaking the definition.
Furthermore let \(S\) be any set with more than two elements.
Now \(I_S\) is a bijection from \(S\) to \(S\).
Note that a 1-ary relation on \(S\) is just a subset of \(S\).
Let \(A \subset B \subseteq S\).
Then \(x \in A \impliess I_S(x) \in B\), but not the other way around, showing
that the dropping the leftward implication in the definition cannot be done.

However, dropping both the leftward implication and the requirement of
bijectivity in the definition of isomorphism one acquires the more general
homomorphism definition.

\begin{definition}
  Let \(S\) and \(\overline{S}\) be sets and let \(R_1,\dots,R_m\) and
  \(\overline{R}_1,\dots,\overline{R}_m\) be relations on \(S\) and
  \(\overline{S}\) respectively.
  If \(\varphi\) is a function from \(S\) to \(\overline{S}\) such that for all
  \(n\)-ary relations \(R_k\) one has
  \[ \x \in R_k \impliess \varphi_n(\x) \in \overline{R}_k, \]
  then \(\varphi\) is said to be an \emph{homomorphism} from \(S\) to
  \(\overline{S}\) with respect to the given relations.
  \(\varphi(S)\) is called the homomorphic image of \(S\) under \(\varphi\).
  If \(\varphi\) is surjective, then \(\varphi\) is said to be an homomorphism
  from \(S\) onto \(\overline{S}\).
\end{definition}

By replacing the equivalences by implications in the proof of proposition
\ref{prop:isom-trans} it is seen that homomorphisms are transitive, that is
given homomorphisms \(\varphi\) and \(\psi\) their composition
\(\varphi \circ \psi\) is a homomorphism.

\begin{definition}
  An \emph{automorphism} on a set \(S\) is an isomorphism from \(S\) to \(S\).
\end{definition}

Note that the identity map, \(I\), is always an automorphism.
A similar definition is the one of an endomorphism.

\begin{definition}
  An \emph{endomorphism} on a set \(S\) is a homomorphism from \(S\) to \(S\).
\end{definition}

\subsection{Morphisms respecting operators}

Recall definition \ref{def:operator} of an operator.
If \(p\) is a binary operator and \(a,b\) are elements of \(S\) then it is
customary to write \(apb\) for \(p(a,b)\).
This notation \(apb\) is motivated by the fact that \(+\) is a binary operator
on \(\setN\) and that it is awkward to write \(+(a,b)\) for the sum of two
natural numbers \(a\) and \(b\).

When a relation is an operator the condition in the definitions simplify.
Note that an \(n\)-ary operator on a set \(S\) is a subset of \(S^n \times S\)
and so a \(n+1\)-ary relation on \(S\).
Let \(S\) and \(T\) be sets let \(p\) and \(q\) be \(n\)-ary operators on \(S\)
and \(T\) respectively.
Let \(\varphi\) be a function.
The simplified condition reads
\begin{equation}\label{eq:morphism-operator}
  \varphi(p(\x)) = q(\varphi_n(\x)) \enspace \text{for all} \enspace \x \in S^n.
\end{equation}
Firstly suppose that \(\varphi\) respects the operators.
Let \(\x \in S^n\) and let \(y = p(\x)\).
Now \((\x,y) \in p\) and since the implication holds one has
\[ \varphi_{n+1}(\x,y) = (\varphi_n(\x),\varphi(y)) \in q \]
By the fact that \(q\) is a function one has
\[ q(\varphi_n(\x)) = \varphi(y) = \varphi(p(\x)), \]
but since \(\x\) is arbitrary this shows \eqref{eq:morphism-operator}.
On the contrary assume \eqref{eq:morphism-operator} holds.
From this the implication in the definition shall be shown.
Therefore suppose \((\x,y) \in p\).
Since \(p\) is a function \(p(\x) = y\) and therefore
\[ \varphi(y) = \varphi(p(\x)) = q(\varphi_n(\x)) \]
by assumption showing that
\[ (\varphi_n(\x),\varphi(y)) = \varphi_{n+1}(\x,y) \in q. \]
Therefore the implication holds.
This shows the following lemma.

\begin{lemma}
  Let \(\varphi\) be a function \(S \to T\).
  \(\varphi\) is a homomorphism respecting \(p\) and \(q\) iff
  \eqref{eq:morphism-operator} holds.
\end{lemma}

The lemma can be augmented to isomorphisms.

\begin{lemma}\label{lem:morphism-group-operator}
  Let \(\varphi\) be a bijection \(S \to T\).
  \(\varphi\) is an isomorphism respecting \(p\) and \(q\) iff
  \eqref{eq:morphism-operator} holds.
\end{lemma}
\begin{proof}
  If \(\varphi\) is an isomorphism then it is also a homomorphism, so that
  \eqref{eq:morphism-operator} holds by the previous lemma.
  Contrarily, assuming \eqref{eq:morphism-operator} the equivalence in the
  definition of isomorphism shall be shown.
  The rightwards implication is true by the previous lemma.
  To show the leftwards one lets \(\varphi_{n+1}(\x,y) \in q\),
  but because this is the same as \((\varphi_n(\x),\varphi(y)) \in q\) and
  since \(q\) is a function one has
  \[ \varphi(y) = q(\varphi_n(\x)) = \varphi(p(\x)) \]
  where also the assumption was used.
  By applying \(\inv{\varphi}\) to both sides one has
  \(y = p(\x)\) in effect showing that \((\x,y) \in p\) as desired.
\end{proof}

The reader may find the simplified condition rather cryptic but when applying it
to binary operators the condition becomes just
\[
  \varphi(xy) = \varphi(x)\varphi(y) \enspace \text{for all} \enspace x,y \in S
\]
where the binary operators \(p\) and \(q\) are denoted by concatenation.
How morphisms treat binary operations shall be studied in the following.

\begin{definition}[Associativity]
  A binary operator \(*\) on \(S\) is said to be associative if
  \((a*b)*c = a*(b*c)\) for all \(a,b,c \in S\).
\end{definition}

\begin{definition}[Commutativity]
  A binary operator \(*\) on \(S\) is said to be commutative if
  \(a*b = b*a\) for all \(a,b \in S\).
\end{definition}

\begin{definition}[Cancelativity]
  A binary operator \(*\) on \(S\) is said to be left cancelative if
  \(c*a = c*b \impliess a = b\) for all \(a,b,c \in S\).
  A binary operator \(*\) is said to be right cancelative if
  \(a*c = b*c \impliess a = b\) for all \(a,b,c \in S\).
  A binary operator which is both left and right cancelative is simply said to
  be cancelative.
\end{definition}

Let \(\varphi\) be a homomorphism from \(S\) to \(U\) with respect to binary
operators \(p\) and \(q\) on \(S\) and \(U\) respectively.
These operations will be denoted by concatenation.
Let \(T \subseteq U\) be the homomorphic image.
Now \(\varphi\) is a homomorphism onto \(T\).
Firstly suppose \(p\) is associative.
Let \(y_1, y_2, y_3 \in T\).
Then \(y_i = \varphi(x_i)\) for some \(x_i \in S\).
One now has
\[
  \begin{split}
    (y_1 y_2) y_3
      &= (\varphi(x_1) \varphi(x_2)) \varphi(x_3) \\
      &= \varphi(x_1 x_2) \varphi(x_3) \\
      &= \varphi((x_1 x_2) x_3) \\
      &= \varphi(x_1 (x_2 x_3)) \\
      &= \varphi(x_1) \varphi(x_2 x_3) \\
      &= \varphi(x_1) (\varphi(x_2) \varphi(x_3)) \\
      &= y_1 (y_2 y_3).
  \end{split}
\]
Therefore homomorphisms preserve associativity.
Similarly if \(S\) is commutative one has
\[
  y_1 y_2
    = \varphi(x_1) \varphi(x_2)
    = \varphi(x_1 x_2)
    = \varphi(x_2 x_1)
    = \varphi(x_2) \varphi(x_1)
    = y_2 y_1,
\]
so the operator \(q\) on \(T\) also commutes.
This proves the following proposition.
\begin{proposition}\label{prop:morp-assoc-comm}
  Homomorphisms preserve associativity and commutativity.
\end{proposition}

However, the same reasoning does not carry over on cancelativity.
Consider the case when the operator on \(S\) is left cancelative
One then has
\[
  y_1 y_2 = y_1 y_3
  \iffs
  \varphi(x_1) \varphi(x_2) = \varphi(x_1) \varphi(x_3)
  \iffs
  \varphi(x_1 x_2) = \varphi(x_1 x_3).
\]
In the last stage one would like to conclude \(x_1 x_2 = x_1 x_3\) so as to be
able to perform cancellation in \(S\), but in general this is not true if
\(\varphi\) is not assumed to be injective.
If \(\varphi\) is injective, then one has the desired equality so that
\(x_2 = x_3\) follows, whence \(y_2 = \varphi(x_2) = \varphi(x_3) = y_3\) and
the isomorphic image is cancelative.
This proves the next proposition.
\begin{proposition}
  Isomorphisms preserve cancelativity.
\end{proposition}

\subsection{Embeddings and transport of structure}

The subsection is closed with a few words on embeddings.

\begin{definition}
  A structure \(S\) is said to be embedded in a structure \(T\) if \(S\) is
  isomorphic to a subset of \(T\).
  The structure \(S\) is said to be extended to \(T\).
\end{definition}

Since isomorphic sets behave precisely the same from an algebraic point of view
it is possible to identify \(S\) with its isomorphic image \(\varphi(S)\) in
\(T\).
Using this identification one can write \(S \subseteq T\).
An algebraican would be satisfied with this.

A worrying set theorist could remove the original elements of \(S\) and replace
them with the corresponding elements of \(T\), in order to really have
\(S \subseteq T\).
Here is the outline.
Let
\[ T' = (T \setminus \varphi(S)) \cup S. \]
Now one indeed has \(S \subseteq T'\).
Consider
\[
  \psi: T' \ni t \mapsto
    \begin{cases}
      t,          &\text{if } t \notin{S}, \\
      \varphi(t), &\text{if } t \in S.
    \end{cases}
\]
A case study shows that \(\psi\) is an isomorphism with respect an old relation
\(R_T\) and the replacement relation
\[ R_{T'} = (R_T \setminus \varphi(S)^2) \cup R_S, \]
where \(R_S\) is the corresponding relation on \(S\).
This manouvre is called transport of structure, since the structure on \(S\) is
in a sense transported to \(T\) (or formally \(T'\)).

The converse of this identification from an injective homomorphism can be done.
Given sets \(S\) and \(T\) such that \(S \subseteq T\) the canonical injection,
\(\iota\), is a injective homomorphism since
\[ \iota(x * y) = x * y = \iota(x) * \iota(y). \]
Therefore one can view the canonical injection as an embedding.
