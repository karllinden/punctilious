\section{An algebraic study of set operations}

To get familiar with the concepts introduced in the previous section, this
subsection is deduced to studying some algebraic structures that can be formed
on a set of sets.
By the general definition a \emph{set automorphism} is, when there are no
relations specified, a bijection on a set.
In this subsection \(U\) denotes any given set.

An example of an set automorphism on \(\powset{U}\) is the complement
\[ U \supseteq A \mapsto \setcomp{A} \subseteq U. \]
Injectivity follows from corollary \ref{cor:set-comp-injective} and
surjectivity follows from
\[ \setcomp{(\setcomp{A})} = A \]
where \(A\) is any set \(A \subseteq U\).
In fact this set automorphism is an isomorphism interchanging \(\cup\) and
\(\cap\).
This follows from the fact that the complement operation is its own inverse and
De Morgan's laws, i.e.
\[
  \setcomp{(A \cup B)} = \setcomp{A} \cap \setcomp{B}
  \quad \text{and} \quad
  \setcomp{(A \cap B)} = \setcomp{A} \cup \setcomp{B}.
\]
Furthermore
\[ A \subseteq B \iffs \setcomp{A} \supseteq \setcomp{B}, \]
so the complement is in fact an isomorphism between
\((\powset{U},\cup,\cap,\subseteq,\supseteq)\) and
\((\powset{U},\cap,\cup,\supseteq,\subseteq)\).

Now automorphisms on \(\powset{U}\) respecting the subset relation, unions and
intersections are studied.
In fact, a consequence of the following theorem is that it is sufficient to
consider only one of these relations.

\begin{proposition}
  An automorphism on \(\powset{U}\) respects all of \(\subseteq\), \(\cup\)
  and \(\cap\) if and only if it respects one of them.
\end{proposition}
\begin{proof}
  Let \(\varphi\) be an automorphism on \(\powset{U}\).
  The proof is carried out by showing
  \[
    \varphi \text{ respects } \cup
    \overset{\text{1}}{\iff}
    \varphi \text{ respects} \subseteq
    \overset{\text{2}}{\iff}
    \varphi \text{ respects } \cap
  \]
  Only equivalence 1 is shown since equivalence 2 follows by an analogous
  argument.

  First suppose \(\varphi\) respects \(\subseteq\).
  Then since
  \[ A \subseteq A \cup B \qandq B \subseteq A \cup B \]
  one has
  \[
    \varphi(A) \subseteq \varphi(A \cup B)
    \qandq
    \varphi(B) \subseteq \varphi(A \cup B).
  \]
  Therefore
  \[ \varphi(A) \cup \varphi(B) \subseteq \varphi(A \cup B). \]
  From the fact that the inverse of \(\varphi\) is also an automorphism
  respecting the subset relation one has the corresponding identity for
  \(\inv{\varphi}\), that is
  \[
    \inv{\varphi}(A) \cup \inv{\varphi}(B) \subseteq \inv{\varphi}(A \cup B).
  \]
  Applying the latter identity on \(\varphi(A)\) and \(\varphi(B)\) gives
  \[ A \cup B \subseteq \inv{\varphi}(\varphi(A) \cup \varphi(B)) \]
  and applying \(\varphi\) on both sides finally gives
  \[ \varphi(A \cup B) \subseteq \varphi(A) \cup \varphi(B). \]
  Therefore
  \[ \varphi(A \cup B) = \varphi(A) \cup \varphi(B) \]
  and \(\varphi\) respects unions.

  Contrarily suppose that \(\varphi\) respects unions.
  Then
  \[
    \begin{split}
      A \subseteq B
        &\iffs A \cup B = B \\
        &\iffs \varphi(A \cup B) = \varphi(B) \\
        &\iffs \varphi(A) \cup \varphi(B) = \varphi(B) \\
        &\iffs \varphi(A) \subseteq \varphi(B),
    \end{split}
  \]
  where proposition \ref{prop:set-inclusion} was used twice.
\end{proof}

Next up is a handy proposition, which is stated generally, although only a
specific case will be used here.
Anyhow the proof is simple so the lemma is proved in all generality here.

\begin{proposition}
  Let \(f\) be a function \(S \to T\) and define
  \[ \varphi: S \supseteq X \mapsto f[X] \subseteq T. \]
  Then the following statements hold.
  \begin{enumerate}
    \item
    If \(f\) is an injection, then \(\varphi\) is an injection.
    \item
    If \(f\) is a surjection, then \(\varphi\) is a surjection.
    \item
    If \(f\) is a bijection, then \(\varphi\) is a bijection.
  \end{enumerate}
\end{proposition}
\begin{proof}
  3 follows from 1 and 2.

  1.
  Let \(A \subseteq S\), \(B \subseteq S\) and \(A \ne B\).
  Then at least one of the sets must contain an element which is not in the
  other.
  Without loss of generality it can be assumed that there exists an \(a \in A\)
  such that \(a \notin B\).
  Then by definition \(f(a) \in f[A]\).
  If \(f(a) \in f[B]\), then since \(a \notin B\) one would have \(f(a) = f(b)\)
  for some \(a \ne b\), which contradicts the injectivity of \(f\).
  Therefore \(f(a) \notin f[B]\), whence \(f[A] \ne f[B]\) follows.

  2.
  Let \(C \subseteq T\) and define
  \[ A = \{s \in S \ssep f(s) \in C\}. \]
  First suppose \(t \in f[A]\).
  Then \(t = f(a)\) for some \(a \in A\), but then by definition of \(A\) one
  has \(t \in C\), so \(f[A] \subseteq C\).
  Next suppose \(t \in C\).
  Then by surjectivity of \(f\) one has \(f(s) = t\) for some \(s \in S\), but
  this means \(s \in A\) and consequently \(t \in f[A]\).
  This means \(C \subseteq f[A]\) and \(f[A] = C\).
  Because \(C\) is arbitrary this proves the surjectivity.
\end{proof}

By translating the proposition to the language of equinumerousity one has the
following two corollaries.
\begin{corollary}
  \(A \preceq B \impliess \powset{A} \preceq \powset{B}\).
\end{corollary}
\begin{corollary}
  \(A \simeq B \impliess \powset{A} \simeq \powset{B}\).
\end{corollary}

Let \(f\) be a bijection on \(U\) and define \(\varphi\) as in the lemma.
It will now be shown that \(\varphi\) is an automorphism respecting the subset
relation, unions and intersections.
It is sufficient to show that \(\varphi\) respects the subset relation.
First assume \(A \subseteq B\).
Suppose \(y \in \varphi(A)\).
Then \(y = f(x)\) for some \(x \in A\).
Then also \(x \in B\), so \(f(x) \in \varphi(B)\), showing
\[ A \subseteq B \impliess \varphi(A) \subseteq \varphi(B). \]
Contrarily assume \(\varphi(A) \subseteq \varphi(B)\).
Suppose \(x \in A\).
Then \(f(x) \in \varphi(A)\) so that \(f(x) \in \varphi(B)\).
By the injectivity of \(f\) one has \(x \in B\) and \(A \subseteq B\) follows.
Therefore
\[ A \subseteq B \iffs \varphi(A) \subseteq \varphi(B). \]
This completes the proof that any bijection on \(U\) induces an automorphism
on \(\powset{U}\) respecting the relations in question.

This reasoning can be reversed in effect showing that every automorphism on
\(\powset{U}\) is induced by a bijection on \(U\).
Let \(\varphi\) be an automorphism on \(\powset{U}\) respecting any of the
relations.
Then \(\varphi\) respects the subset relation.
A direct consequence of this is that \(\varphi(\varnothing) = \varnothing\).
This follows from the fact that
\[ \varnothing \subseteq A \iffs \varphi(\varnothing) \subseteq \varphi(A), \]
where the left hand side is always true and consequently also the right hand
side.
In particular there exists an \(A\) such that \(\varphi(A) = \varnothing\).
Thus, \(\varphi(\varnothing) \subseteq \varnothing\) and
\(\varphi(\varnothing) = \varnothing\) follows.
Now let \(x \in U\) and let \(A = \varphi(\{x\})\).
Firstly \(A \ne \varnothing\), since
\[
  A = \varnothing
  \impliess
  A \subseteq \varnothing
  \impliess
  \{x\} \subseteq \varnothing
  \iffs
  x \in \varnothing
\]
and the latter is a contradiction.
Let \(y,z \in A\).
Then
\[ \{y\} \subseteq A \qandq \{z\} \subseteq A \]
Because \(\varphi\) is surjective there exists \(B\) and \(C\) such that
\[ \varphi(B) = \{y\} \qandq \varphi(C) = \{z\}. \]
By the assumption on \(\varphi\) both \(B\) and \(C\) are subsets of \(\{x\}\).
\(B\) and \(C\) cannot be empty.
Therefore the only possibility is \(B = C = \{x\}\), but then \(y = z\).

It has been shown that for every \(x \in U\) the image of \(\{x\}\) under
\(\varphi\) contains exactly one element, say \(y \in U\).
Therefore one can define \(f: U \to U\) such that
\[ \varphi(\{x\}) = \{f(x)\} = f[\{x\}]. \]
Suppose \(f(x) = f(x')\).
Then
\[ \varphi(\{x\}) = \{f(x)\} = \{f(x')\} = \varphi(\{x'\}) \]
and \(x = x'\) follows by the injectivity of \(\varphi\).
Therefore \(f\) is injective.
Next let \(y \in U\).
By the surjectivity of \(\varphi\), there exists a \(B\) such that
\[ \varphi(B) = \{y\}. \]
Clearly \(B \ne \varnothing\).
Suppose \(x,z \in B\).
Then by the assumption on \(\varphi\)
\[
  \{x\} \subseteq B
  \impliess
  \varphi(\{x\}) = \{f(x)\} \subseteq \{y\}
  \impliess
  f(x) = y.
\]
Similar holds for \(z\) so that \(f(x) = f(z)\) and \(x = z\) by the
injectivity.
Therefore \(B\) contains precisely one element, say \(x\).
It follows that \(f(x) = y\) which proves that \(f\) is surjective.
Therefore \(f\) is a bijection \(U \to U\).

It has been demonstrated that given an automorphism \(\varphi\) on
\(\powset{U}\) it is possible to construct a related bijection \(f: U \to U\).
However, it is not yet clear that \(\varphi\) is actually induced by this
\(f\).
It must therefore be shown that \(\varphi(A) = f[A]\).
First suppose \(y \in \varphi(A)\).
Since \(f\) is surjective \(f(x) = y\) for some \(x\).
Thus by the definition of \(f\) and the assumption on \(\varphi\) one has
\[
  f(x) \in \varphi(A)
    \iffs \{f(x)\} \subseteq \varphi(A)
    \iffs \varphi(\{x\}) \subseteq \varphi(A)
    \iffs \{x\} \subseteq A
    \iffs x \in A,
\]
but then \(y \in f[A]\).
Next suppose \(y \in f[A]\).
Then \(y = f(x)\) for some \(x \in A\), which by the equivalence above means
\(y \in \varphi(A)\).
Hence \(\varphi(A) = f[A]\) and \(\varphi\) is in fact the automorphism induced
by \(f\).

Before concluding the findings so far it is noted that an automorphism of the
kind discussed here must necessarily respect complements, that is
\[ \varphi(\setcomp{A}) = \setcomp{\varphi(A)}. \]
This follows from the fact that \(\varphi\) is induced by a bijection \(f\) on
\(U\).
One therefore has
\[
  \begin{split}
    y \in \varphi(\setcomp{A})
      &\iffs y \in f[\setcomp{A}] \\
      &\iffs \inv{f}(y) \in \setcomp{A} \\
      &\iffs \inv{f}(y) \notin A \\
      &\iffs y \notin f[A] \\
      &\iffs y \in \setcomp{f[A]} \\
      &\iffs y \in \setcomp{\varphi(A)}.
  \end{split}
\]
A consequence of this is that an automorphism which respects the subset
relation, unions and intersections must also respect the set difference.
This is because
\[
  \varphi(A \setminus B)
    = \varphi(A \cap \setcomp{B})
    = \varphi(A) \cap \varphi(\setcomp{B})
    = \varphi(A) \cap \setcomp{\varphi(B)}
    = \varphi(A) \setminus \varphi(B).
\]
In fact any automorphism which respects the set difference must respect
intersections, and consequently all relations.
This is a consequence of corollary \ref{cor:set-intersect-with-minus}.
\[
  \begin{split}
    \varphi(A \cap B)
      &= \varphi(A \setminus (A \setminus B)) \\
      &= \varphi(A) \setminus \varphi(A \setminus B) \\
      &= \varphi(A) \setminus (\varphi(A) \setminus \varphi(B)) \\
      &= \varphi(A) \cap \varphi(B).
  \end{split}
\]

Finally it is possible to present the main result of this section.
\begin{theorem}
  A bijection on any set \(U\) induces an automorphism on \(\powset{U}\) that
  respects
  \begin{enumerate}
    \item subset relations,
    \item unions,
    \item intersections, and
    \item set differences.
  \end{enumerate}
  An automorphism on \(\powset{U}\) that respects any one of the above is of
  this induced kind.
  Furthermore these automorphisms respect set complements.
\end{theorem}

Note that it is not sufficient for an automorphism to respect set complements to
be of the induced kind.
This is easy to see, since indeed
\[ \varphi: A \mapsto \setcomp{A} \]
respects set complements since
\[ \varphi(\setcomp{A}) = \setcomp{(\setcomp{A})} = \setcomp{\varphi(A)}. \]
However, the subset relation cannot be respected unless \(U = \varnothing\),
because
\[ \varphi(\varnothing) = \setcomp{\varnothing} = U \]
and \(\varphi(\varnothing) = \varnothing\) for any automorphism respecting
subset relations.

\subsection{The group under symmetric difference}

\begin{definition}
  The symmetric difference \(A \symdiff B\) of the sets \(A\) and \(B\) is
  defined as
  \[ A \symdiff B \defeq (A \setminus B) \cup (B \setminus A). \]
\end{definition}

Let \(U\) be a set.
It shall be shown that \(\powset{U}\) forms a group under the symmetric
difference.
If \(A,B \subseteq U\), then clearly \(A \symdiff B \subseteq U\).
Therefore the symmetric difference is an operator on \(\powset{U}\).

The proof of the following proposition is tedious and boring.
See
\begin{center}
  \url{https://proofwiki.org/wiki/Symmetric_Difference_is_Associative}
\end{center}
for a proof.
\begin{proposition}
  \((A \symdiff B) \symdiff C = A \symdiff (B \symdiff C)\).
\end{proposition}

Directly by the definition it is seen that
\[ A \symdiff B = B \symdiff A \]
which means that the symmetric difference commutes.
To show that \(\powset{U}\) is a group under \(\symdiff\) one notes that
\(\varnothing\) is an identity since
\[
  \varnothing \symdiff A
    = (\varnothing \setminus A) \cup (A \setminus \varnothing)
    = A.
\]
Also one has
\[ A \symdiff A = (A \setminus A) \cup (A \setminus A) = \varnothing, \]
so that \(A\) is always invertible (with itself as inverse).
Therefore it is a group.

Since \(\symdiff\) was defined using only the ordinary set operations it is seen
by means of the main theorem of the previous section that every bijection
\(U \to U\) induces a group automorphism on \((\powset{U},\symdiff)\).
These induced automorphisms are not the only ones since the function defined so
that
\begin{gather*}
  \varnothing \mapsto \varnothing \\
  \{1\} \mapsto \{1,2\} \\
  \{2\} \mapsto \{1\} \\
  \{1,2\} \mapsto \{2\}
\end{gather*}
is an automorphism, but is clearly not of the induced kind.
