#  Makefile
#
#  Copyright 2014-2017 Karl Lindén <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

LATEX     ?= latex
LATEXARGS ?= -halt-on-error

chapfiles := \
	symbolic-boolean-algebra.in.tex \
	logic.in.tex \
	set-theory.in.tex \
	functions-and-relations.in.tex \
	natural-numbers.in.tex \
	more-set-theory.in.tex \
	inductive-sets.in.tex \
	morphisms.in.tex \
	associative-structures.in.tex \
	an-algebraic-study-of-set-operations.in.tex \
	ordinals.in.tex \
	more-order-theory.in.tex \
	rings-and-fields.in.tex \
	real-numbers.in.tex \
	topology.in.tex
texfiles := \
	$(chapfiles:.in.tex=.tex) \
	punctilious.tex
dvifiles := $(texfiles:.tex=.dvi)
pdffiles := $(texfiles:.tex=.pdf)
psfiles  := $(texfiles:.tex=.ps)

cleanfiles := $(chapfiles:.in.tex=.tex)
cleanfiles += $(dvifiles)
cleanfiles += $(pdffiles)
cleanfiles += $(psfiles)
cleanfiles += $(texfiles:.tex=.out.ps)
cleanfiles += $(chapfiles:.tex=.aux) $(texfiles:.tex=.aux)
cleanfiles += $(texfiles:.tex=.log)
cleanfiles += $(texfiles:.tex=.out)
cleanfiles += $(texfiles:.tex=.toc)

all: pdf .gitignore
dvi: $(dvifiles)
pdf: $(pdffiles)
ps: $(psfiles)

clean:
	-rm -f $(cleanfiles)

.PHONY: all dvi pdf ps clean

.DELETE_ON_ERROR:

$(chapfiles:.in.tex=.tex): %.tex: chapter.tex.in
	sed -e 's/@CHAPTER@/$*/' chapter.tex.in > $@

$(chapfiles:.in.tex=.dvi): %.dvi: %.tex %.in.tex common.tex
	$(LATEX) $(LATEXARGS) $<
	while grep -q 'Rerun to get ' $*.log ; do $(LATEX) $(LATEXARGS) $< ; done
	while grep -q 'No file $*.toc' $*.log ; do $(LATEX) $(LATEXARGS) $< ; done

punctilious.dvi: punctilious.tex $(chapfiles) common.tex title-footer.tex
	$(LATEX) $(LATEXARGS) $<
	while grep -q 'Rerun to get ' $*.log ; do $(LATEX) $(LATEXARGS) $< ; done
	while grep -q 'No file $*.toc' $*.log ; do $(LATEX) $(LATEXARGS) $< ; done

%.ps: %.dvi
	dvips $< -o $@

%.pdf: %.ps
	ps2pdf $< $@

.gitignore: Makefile
	sed -e '/^.*.tex$$/d' -i .gitignore; \
	printf '%s\n' $(chapfiles:.in.tex=.tex) >> .gitignore
