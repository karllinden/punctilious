\section{More Order Theory}

In chapter \ref{chap:functions-and-relations} many properties of ordered
structures were established.
This chapter extends the order theory by introducing results that follows by
induction.
Furthermore the theory of lattices is described and parallels are drawn to the
concepts discussed in chapter \ref{chap:associative-structures}.
After that a few results and definitions about totally ordered sets are stated.

\subsection{More on Maximal Elements}

A poset need not have a maximal element, as the following example shows.

\begin{example}
  The natural numbers, \(\setN\), is a toset, and therefore also a poset, under
  the ordinary order \(\le\).
  That \(\setN\) has no maximal element follows from that for all
  \(n \in \setN\) one has \(n < n + 1\).
\end{example}

Note that \(\setN\) is infinte.
It would not have been possible to construct an example of a poset without
maximal element with only finite posets at disposal, which the following
proposition shows.

\begin{proposition}
  Every finite poset has a maximal element.
\end{proposition}
\begin{proof}
  For a poset \(P_1 = \{x\}\) with a single element the only partial order on
  \(P_1\) is \(\{(x,x)\}\) and in this order \(x\) is maximal.

  Suppose every finite poset \(P_n\) with \(n\) elements has a maixmal element.
  Let \(P_{n+1}\) be a poset with \(n+1\) elements.
  Let \(z \in P_{n+1}\) and let \(P_n = P_{n+1} \setminus \{z\}\).
  By assumption \(P_n\) has a maximal element.
  If not \(x < z\) then \(x\) is the maximal element of \(P_{n+1}\), and there
  is nothing to prove.
  Otherwise if \(x < z\), \(z\) is the maximal element, since if \(z < y\) for
  some \(y \in P_{n+1}\) then \(z < y\) for some \(y \in P_n\).
  It then follows that \(x < y\) contrary to the fact that \(x\) is maximal in
  \(P_n\).
  Therefore \(P_{n+1}\) has a maximal element.

  The proof is completed by the induction principle.
\end{proof}

In the section about partially ordered sets it was concluded that if there are
multiple maximal elements then there can be no maximum.
However, there are sets with unique maximal elements, but which lacks a maximum.

\begin{example}
  Let \(P = \setN \cup \{(x,x)\}\) where \(x \notin \setN\).
  Define
  \[ (\precsim) = (\le) \cup \{(x,x)\}, \]
  so that
  \[ a \precsim b \iffs a \le b \text{ or } a = b = x. \]
  It is easy to see that \(\precsim\) is reflexive.
  That it is transitive follows from that if \(a \precsim b\) and
  \(b \precsim c\) then one of the four cases
  \begin{romanenum}
    \item \(a \le b\) and \(b \le c\),
    \item \(a \le b\) and \(b = c = x\),
    \item \(a = b = x\) and \(b \le c\),
    \item \(a = b = x\) and \(b = c = x\),
  \end{romanenum}
  occur, but the cases (ii) and (iii) are contradictions since
  \(x \notin \setN\).
  In the cases (i) and (iv) transitivity is clear.
  Lastly \(\precsim\) is antisymmetric.
  If \(a \precsim b\) and \(b \precsim a\) then by a similar argument as above
  it holds that either \(a \le b\) and \(b \le a\) or \(a = b = x\).
  In either case antisymmetry is clear.
  Now \(x\) is maximal since \(x \precsim y\) implies \(x = y\).
  This is because
  \[ (x,y) \in (\le) \cup \{(x,x)\} \iffs (x,y) \in (\le) \vee (x,y) = (x,x) \]
  and the  \(x \le y\) is impossible since \(x \notin \setN\).
  By \(n < n + 1\) for all \(n \in \setN\) it follows that \(x\) is the unique
  maximal element of \(P\).
  That \(x\) is not a maximum follows from that \((n,x) \notin (\precsim)\) for
  any \(n \in \setN\).
\end{example}

In fact it is necessary to consider infinite sets too be able to cook up a poset
with a unique maximal element but no maximum.

\begin{proposition}
  If \((P,\le)\) is a finite poset with a unique maximal element \(x\) then
  \(x\) is a maximum.
\end{proposition}
\begin{proof}
  Consider a poset \(P_1 = \{x\}\) with only one element.
  The only partial order on \(P_1\) is \(\{(x,x)\}\) and clearly \(x\) is both a
  unique maximal element and maximum.

  Suppose whenever \(x\) is a unique maximal element in a poset \(P_n\) with
  \(n\) elements, then \(x\) is a maximum.
  Consider a poset \(P_{n+1}\) with \(n+1\) elements that has the unique maximal
  element \(x\).
  Choose \(z \in P_{n+1}\) such that \(z \ne x\) and let
  \(P_n = P_{n+1} \setminus \{z\}\).
  Now \(P_n\) is a poset with \(n\) elements and \(x\) is a maximal element of
  \(P_n\).
  Let \(x'\) be a maximal element of \(P_n\) and assume \(x' \ne x\).
  Then it must hold that \(x' < z\), because if not then \(x'\) is also a
  maximal element of \(P_{n+1}\) contrary to the uniqueness.
  In this case \(z\) is a maximal element of \(P_{n+1}\) since if \(z < y\)
  for some \(y \in P_{n+1}\) then \(y \in P_n\).
  Therefore by transitivity \(x' < y\) for some \(y \in P_n\) contrary to the
  assumption that \(x'\) is maximal.
  This again contradicts that \(x\) is a unique maximal element of
  \(P_{n+1}\).
  Hence, the assumption that \(x' \ne x\) is false and \(x\) is the unique
  maximal element of \(P_n\).
  By the induction hypothesis \(x\) is the maximum of \(P_n\).
  When it has been shown that \(z \le x\) it follows that \(x\) is the maximum
  of \(P_{n+1}\) and the induction step is complete.
  It cannot hold that \(z < y\) for no \(y \in P_n\) since then \(z\) is a
  maximal element of \(P_{n+1}\).
  Therefore \(z < y\) for some \(y \in P_n\).
  Because \(x\) is the maximum of \(P_n\) one has \(y \le x\).
  By transitivity \(z \le x\).

  Finally, the result follows by the induction principle.
\end{proof}

\begin{proposition}
  If \((T,\le)\) is a totally ordered set and \(x\) is a maximal element of
  \(T\), then \(x\) is the maximum of \(T\).
\end{proposition}
\begin{proof}
  Let \(y \in T\).
  By trichotomy \(x < y\), \(x = y\) or \(y < x\).
  Since \(x\) is maximal the first cannot occur.
  Thus, \(y \le x\) and it follows that \(x\) is a maximum.
\end{proof}

By duality these results on maximas an maximums carry over to minimas and
minimal elements.

\subsection{Lattices}

\begin{definition}
  Let \((P,\le)\) be a poset.
  An element \(u \in P\) is said to be an upper bound of a subset \(S\) of \(P\)
  if \(s \le u\) for all \(s \in S\).
  Similarly an element \(l \in P\) is said to be a lower bound of \(S\) if
  \(l \le s\) for all \(s \in S\).
\end{definition}

A subset of a poset may or may not have an upper bound.
\begin{example}
  Consider the poset
  \[ \Omega = \{\{1\}, \{2\}, \{3\}, \{1,2\}\} \]
  under inclusion.
  Then \(\{1,2\}\) is an upper bound of \(\{\{1\},\{2\}\}\).
  The subset \(\{\{2\}, \{3\}\}\) has no upper bound.
\end{example}

\begin{definition}
  A subset \(S \subseteq P\) is said to be bounded from above if there exists an
  upper bound of \(S\).
  Similarly \(S\) is said to be bounded from below if there exists a lower
  bound of \(S\).
  A subset \(S \subseteq O\) is said to be bounded if there exist both a lower
  and an upper bound.
\end{definition}

Given a set \(S \subseteq P\) one can form the set of upper bounds \(U\).
If there is a least element of \(U\) then one can speak of a least upper bound
of \(S\).
As shown before this least upper bound is unique since it is a minimum.
Similarly one can create the set \(L\) of lower bounds and consider the greatest
lower bound if such an element exists.
Now a lattice can be defined.

\begin{definition}
  A join-semilattice is a poset such that every subset \(\{a,b\}\) has a least
  upper bound, denoted by \(a \vee b\) (``\(a\) join \(b\)'').
  A meet-semilattice is a poset such that every subset \(\{a,b\}\) has a
  greatest lower bound, denoted by \(a \wedge b\) (``\(a\) meet \(b\)'').
  A lattice is a both a join-semilattice and a meet-semilattice.
\end{definition}

Clearly \(\vee\) and \(\wedge\) are binary operators on \(L\).

By duality \(l\) is the least upper bound of \(a\) and \(b\) in \((L,\le)\)
iff \(l\) is the greatest lower bound of \(a\) and \(b\) in the dual poset
\(\dual{L}\).
Therefore \((L,\le)\) is a join-semilattice iff \(\dual{L}\) is a
meet-semilattice and
\[ a \vee b = l \text{ in } L\iff a \wedge b = l \text{ in } \dual{L}. \]
By letting \(\le\) and \(\ge\) swap roles it is seen that
\[ a \wedge b = g \text{ in } L \iff a \vee b = g \text{ in } \dual{L}. \]
Therefore a result that holds in a join-semilattice holds in a meet-semilattice
by replacing \(\vee\) with \(\wedge\), since the identity function is an
isomorphism replacing \(\vee\) with \(\wedge\).
Similarly a formula that holds in a lattice will also hold if \(\vee\) and
\(\wedge\) are interchanged, again since the identity map \(L \to L\) is an
isomorphism between \(((L,\le),\vee,\wedge)\) and \(((L,\ge),\wedge,\vee)\).
This is the formal basis of the principle of duality for lattices.
Note that this map reverses order.

\begin{example}
  Given any set \(X\), the power set \(\powset{X}\) is lattice under
  \(\subseteq\).
  The least upper bound of \(A \subseteq X\) and \(B \subseteq X\) is
  \(A \cup B\).
  This is because if \(A \subseteq C\) and \(B \subseteq C\), then because the
  union preserves order
  \[ A \cup B \subseteq C \cup C = C \]
  so \(A \cup B\) is indeed the least upper bound.
  Similarly the greatest lower bound of \(A\) and \(B\) is \(A \cap B\).
\end{example}

\begin{proposition}
  For a join-semilattice \((L,\le)\) the following are true for all
  \(a,b,c \in L\).
  \begin{romanenum}
    \item \(a \le a \vee b\).
    \item \(b \le a \vee b\).
    \item If \(a \le c\) and \(b \le c\), then \(a \vee b \le c\).
    \item \(a \vee a = a\).
    \item \(a \vee b = b \vee a\).
    \item \((a \vee b) \vee c = a \vee (b \vee c)\).
  \end{romanenum}
\end{proposition}
\begin{proof}
  Only the last property is not a direct consequence of the definition.
  Let \(d = (a \vee b) \vee c\) and \(e = a \vee (b \vee c)\).
  By (i) one has \(a \vee b \le d\) and by (ii) one has \(c \le d\).
  Again by (i) and (ii) and transitivity one has \(a \le d\) and \(b \le d\).
  Thus, \(a,b,c \le d\).
  Similarly \(a,b,c \le e\).
  Now \(a \vee b \le e\) by (iii).
  Again by (iii) \((a \vee b) \vee c \le e\), so that \(d \le e\).
  A similar argument shows \(e \le d\), which completes the proof.
\end{proof}

By duality one has the following proposition.

\begin{proposition}
  For a meet-semilattice \((L,\le)\) the following are true for all
  \(a,b,c \in L\).
  \begin{romanenum}
    \item \(a \wedge b \le a\).
    \item \(b \wedge b \le b\).
    \item If \(c \le a\) and \(c \le b\), then \(c \le a \wedge b\).
    \item \(a \wedge a = a\).
    \item \(a \wedge b = b \wedge a\).
    \item \((a \wedge b) \wedge c = a \wedge (b \wedge c)\).
  \end{romanenum}
\end{proposition}

\begin{proposition}
  \(a \vee b = b \iffs a \le b\).
\end{proposition}
\begin{proposition}
  \(a \wedge b = b \iffs b \le a\).
\end{proposition}
\begin{proof}
  Only the first proposition is shown since the latter follows by duality.
  (\(\Rightarrow\)) follows by
  \[ a \le a \vee b = b. \]
  If \(a \le b\), then \(b\) is clearly the least upper bound for \(\{a,b\}\).
\end{proof}

Inspired by the proposition one can make an alternative definition of what a
lattice is, that does not rely on the order.
Everything up til now has been based on the order properties.
By discarding the order and seeing a lattice as an algebraic structure one ends
up with the following alternative definition of a lattice.

\begin{definition}
  A lattice is a triple \((L,\vee,\wedge)\) such that the following statements
  are true for all \(a,b,c \in L\).
  \begin{enumerate}[twocol]
    \item
    \(x \vee x = x\).
    \item
    \(x \vee y = y \vee x\).
    \item
    \(x \vee (y \vee z) = (x \vee y) \vee z\).
    \item
    \(x \wedge x = x\).
    \item
    \(x \wedge y = y \wedge x\).
    \item
    \(x \wedge (y \wedge z) = (x \wedge y) \wedge z\).
  \end{enumerate}
  A join-semilattice is a pair \((L,\vee)\) such that 1-3 hold.
  A meet-semilattice is a pair \((L,\wedge)\) such that 4-6 hold.
\end{definition}

It is a must to check that the definitions are equivalent.
Therefore assume \((L,\vee)\) is a join-semilattice as defined by the
alternative definition.
Define the relation \(\le\) on \(L\) such that
\[ a \le b \iffs a \vee b = b. \]
If the join-semilattice \((L,\vee)\) is the lattice constructed by the order
\(\le\) then this definition just returns the constructing order.
It must be shown that \(\le\) is a partial order.
That \(a \le a\) follows from that \(a \vee a = a\).
Suppose \(a \le b\) and \(b \le a\).
Then
\[ a = b \vee a = a \vee b = b. \]
Lastly suppose \(a \le b\) and \(b \le c\).
Then
\[ a \vee c = a \vee (b \vee c) = (a \vee b) \vee c = b \vee c = c \]
showing that \(a \le c\).

By the algebraic lattice definition it is clear what lattice morphisms should
be, namely a morphism that respects the meet and join.
% FIXME: generalize the isomorphism theorem
By a general theorem on homomorphisms the homomorphic image of a lattice is a
lattice.

\begin{proposition}
  Let \(L_1\) and \(L_2\) be join-semilattices.
  A homomorphism \(L_1 \to L_2\) is order preserving.
\end{proposition}
\begin{proof}
  A homomorphism \(\varphi\) has the property that
  \[ \varphi(x \vee y) = \varphi(x) \vee \varphi(y). \]
  Suppose that \(x \le y\).
  Then \(x \vee y = y\).
  By applying \(\varphi\) and using the above property one has
  \[ \varphi(x) \vee \varphi(y) = \varphi(y) \]
  but this means \(\varphi(x) \le \varphi(y)\).
\end{proof}

Obviously the proposition is true for meet-semilattices and lattices too.
Consequently, a semilattice isomorphism is order preserving with order
preserving inverse, so that the isomorphic lattices are in addition order
isomorphic.
The converse is true, atleast for isomorphisms.

\begin{proposition}
  Let \(L_1\) and \(L_2\) be join-semilattice.
  Any order isomorphism \(L_1 \to L_2\) is a semilattice isopmorphism.
\end{proposition}
\begin{proof}
  Let \(\varphi: L_1 \to L_2\) is be order preserving.
  From \(a \le a \vee b\) and \(b \le a \vee b\) follows
  \[
    \varphi(a) \le \varphi(a \vee b) \qandq \varphi(b) \le \varphi(a \vee b).
  \]
  Consequently
  \[ \varphi(a) \vee \varphi(b) \le \varphi(a \vee b). \]
  Therefore \(\varphi(a)\) is an upper bound of \(\{\varphi(a),\varphi(b)\}\).
  To show that it is the least let \(c'\) be an upper bound.
  One has
  \[
    \varphi(a) \le c' \impliess a \le \inv{\varphi}(c')
    \qandq
    \varphi(b) \le c' \impliess b \le \inv{\varphi}(c').
  \]
  It follows that
  \[ a \vee b \le \inv{\varphi}(c'), \]
  so that
  \[ \varphi(a \vee b) \le c'. \]
  Hence, \(\varphi(a \vee b)\) is the least upper bound, completing the proof.
\end{proof}

For homomorphisms this result is not true.
Define an order homomorphism by
\[ \varphi: \mathcal{P}(\{1,2\}) \ni S \mapsto |S| \in \{0,1,2\}, \]
where the sets have their usual partial orders.
One has
\[
  1
    = 1 \vee 1
    = \varphi(\{1\}) \vee \varphi(\{2\})
    \ne \varphi(\{1\} \cup \{2\})
    = \varphi(\{1,2\})
    = 2.
\]

By the algebraic description of a lattice it is clear by using the results on
semigroups one has that if \(S = \{a_1,\dots,a_n\}\) is a finite subset of a
lattice then it has a least upper bound as well as a greatest lower bound
denoted by
\[ \bigvee S = \bigvee_{a \in S} a = \bigvee_{k=1}^n a_n \]
and
\[ \bigwedge S = \bigwedge_{a \in S} a = \bigwedge_{k=1}^n a_n \]
respectively.
To make the algebraic structure a monoid one needs an element such that
\[ a \vee 0 = a \]
for all \(a \in L\) and similarly for the meet.

The previous definitions of boundedness carry over to the entire (semi)lattice
as well.
The lower bound is usually called \(0\) and the upper bound \(1\).

\begin{proposition}
  If \(L\) is a join-semilattice, bounded from above by \(1\), then
  \(a \vee 1 = 1\) for all \(a \in L\).
\end{proposition}
\begin{proposition}
  If \(L\) is a meet semi-lattice, bounded from below by \(0\), then
  \(a \wedge 0 = 0\) for all \(a \in L\).
\end{proposition}
\begin{proof}
  Since \(1\) is an upper bound of \(L\) one has \(a \le 1\) and \(1 \le 1\).
  Therefore \(a \vee 1 \le 1\).
  Clearly \(a \vee 1 < 1\) is impossible.
  Thus \(a \vee 1 = 1\).
\end{proof}

Next is the property that make properly bounded (semi)lattices monoids.

\begin{proposition}
  If \(L\) is a join-semilattice, bonded from below by \(0\), then
  \(a \vee 0 = a\) for all \(a \in L\).
\end{proposition}
\begin{proposition}
  If \(L\) is a meet-semilattice, bounded from above by \(1\), then
  \(a \wedge 1 = a\) for all \(a \in L\).
\end{proposition}
\begin{proof}
  \(a\) is an upper bound of \(\{a,0\}\) since \(a \le a\) and \(0 \le a\).
  Thus, \(a \vee 0 \le a\).
  Equality holds since \(a \vee 0 < a\) is impossible.
\end{proof}

With the proposition one has that a join-semilattice \((L,\vee,0)\) where \(0\)
is a lower bound is a monoid.
Therefore it makes sense to write
\[ \bigvee \varnothing = 0 \]
and similarly for meet-semilattices.

Note that every (semi)lattice can be turned into a bounded (semi)lattice by
introducing artificial lower and upper bounds.

In lattices every finite subset has a least upper bound (greatest lower bound).
There is of course a natural generalization to this.

\begin{definition}
  A join-semilattice is said to be complete if every subset has a least upper
  bound.
  Similarly, a meet-semilattice is complete if every subset has a greatest lower
  bound.
  A lattice is defined to be complete if every subset has both a least upper
  bound and a greatest lower bound.
\end{definition}

Not only arbitrarily large sets shall have suprema or infinima.
With this definition the empty set must also have a least upper bound or
greatest lower bound.
Therefore one has the following property.

\begin{proposition}
  A complete (semi-)lattice is bounded.
\end{proposition}
\begin{proof}
  The result is shown on a join-semilattice.
  A similar argument applies to meet-semilattices and finally the result follows
  for lattices by joining the arguments.

  By the completeness \(\varnothing\) has a least upper bound.
  Since the implication
  \[ x \in \varnothing \impliess x \le y \]
  is true for all \(y\) in the lattice every element is an upper bound.
  Therefore the lattice must have a least element, which completes the argument.
\end{proof}

For a lattice to be complete a more relaxed condition is sufficient.

\begin{proposition}
  A lattice is complete iff one of the following holds.
  \begin{romanenum}
    \item Every subset has a least upper bound.
    \item Every subset has a greatest lower bound.
  \end{romanenum}
\end{proposition}
\begin{proof}
  It shall be shown that (i) implies (ii).
  That the reverse implication holds follows by duality (or a similar argument).
  Denote the lattice by \(L\) and let \(S \subseteq L\).
  Form
  \[ T = \{t \in L \ssep t \le s \text{ for all } s \in S\}. \]
  Now \(T\) has a least upper bound, say \(b\).
  Suppose \(s \in S\).
  Then \(s\) is an upper bound for \(T\) so that \(b \le s\) by minimality.
  Therefore \(b\) is a lower bound of \(S\).
  Let \(l\) be a lower bound of \(S\).
  Then \(l \in T\), so that \(l \le b\), by the fact that \(b\) is an upper
  bound of \(T\).
  Hence, \(b\) is the greatest lower bound of \(S\) and the proof is complete.
\end{proof}

Sometimes it is too strict to require every set to have join and meet, so one
can introduce bounded completeness.

\begin{definition}
  A join semi-lattice is said to be bounded complete if every non-empty subset
  with an upper bound has a least upper bound.
  A meet semi-lattice is said to be bounded complete if every non-empty subset
  with a lower bound has a greatest lower bound.
  A lattice is said to be bounded complete if the two above definitions hold.
\end{definition}

This notion of completeness does not force the lattice to be bounded for it to
be complete in this sense.
Note that a complete lattices is bounded complete, so bounded completeness is
substantially weaker.
Also for bounded completeness a more relaxed condition can be formulated for
lattices.

\begin{proposition}
  A lattice is complete iff one of the following holds.
  \begin{romanenum}
    \item Every non-empty subset with an upper bound has a least upper bound.
    \item Every non-empty subset with a lower bound has a least lower bound.
  \end{romanenum}
\end{proposition}
\begin{proof}
  A lattice \(L\) is complete iff (i) and (ii) holds simultaneously.
  Therefore it suffices to show that (i) and (ii) are equivalent.
  Only the implication (i) \(\impliess\) (ii) will be shown because the other
  one follows by duality (or a similar argument).

  Let \(L\) be a lattice where (i) holds, and suppose \(S\) is a non-empty
  subset with a lower bound.
  Let
  \[ R = \{l \in L \ssep l \le s \text{ for all } s \in S\}. \]
  Now \(R\) is non-empty by the assumption that \(S\) has a lower bound.
  Also \(R\) is bounded from above, since
  \[ l \in R \impliess l \le s \]
  for all \(s \in S\).
  Since \(S\) non-empty there exists an element \(s' \in S\).
  For this particular \(s'\) it holds that \(l \le s'\) for all \(l \in R\).
  This shows that \(R\) has an upper bound.
  By (i) \(R\) has a least upper bound, say \(b\).

  Next it must be shown that \(b\) is indeed the greatest lower bound of \(S\).
  Let \(s \in S\).
  By the same argument as before \(s\) is an upper bound of \(R\) so that
  \(b \le s\) since \(b\) is the least upper bound of \(R\).
  This shows that \(b\) is a lower bound of \(S\).
  Let \(l\) be a lower bound of \(S\).
  Then \(l \in R\), so that \(l \le b\) since \(b\) is an upper bound of \(R\),
  but then \(b\) is the greatest lower bound of \(S\).
\end{proof}

\begin{lemma}\label{lem:order-lattice-upper-bound}
  Let \(L_1\) and \(L_2\) be lattices and suppose \(f: L_1 \to L_2\) is an order
  isomorphism.
  Suppose \(S \subseteq L_1\).
  Then \(u \in L_1\) is an upper bound of \(S\) if and only if \(f(u) \in L_2\)
  is an upper bound of \(f(S)\).
\end{lemma}
\begin{proof}
  First let \(u \in L_1\) be an upper bound of \(S\).
  Let \(y \in f(S)\).
  Then \(y = f(x)\) for some \(x \in S\).
  Since \(u\) is an upper bound of \(S\) one has \(x \le u\) and consequently
  \(y = f(x) \le f(u)\), showing that \(f(u)\) is an upper bound of \(f(S)\).

  If \(f(u)\) is an upper bound of \(f(S)\) then by applying \(\inv{f}\) which
  is also an order isomorphism one has by what has been done that \(u\) is a
  least upper bound of \(S\).
\end{proof}

\begin{lemma}\label{lem:order-lattice-completeness}
  Let \(L_1\) and \(L_2\) be lattices and suppose \(f: L_1 \to L_2\) is an order
  isomorphism.
  Suppose \(S \subseteq L_1\).
  Then \(u \in L_1\) is a least upper bound of \(S\) if and only if
  \(f(u) \in L_2\) is a least upper bound of \(f(S)\).
\end{lemma}
\begin{proof}
  First let \(u \in L_1\) be a least upper bound of \(S\).
  It has been shown that \(f(u)\) is an upper bound of \(f(S)\).
  Suppose \(w \in L_2\) is an upper bound of \(f(S)\).
  Let \(f(v) = w\).
  Then \(f(x) \le f(v)\) for all \(x \in S\) since \(f(v)\) is an upper bound
  of \(f(S)\).
  Since \(f\) is an order isomorphism \(x \le v\).
  Thus, \(v\) is an upper bound of \(S\), so that \(u \le v\).
  Now \(f(u) \le f(v)\), showing that \(f(u)\) is the least upper bound of
  \(f(S)\).

  If \(f(u)\) is a least upper bound of \(f(S)\) then by applying \(\inv{f}\)
  which is also an order isomorphism one has by what has been done that \(u\) is
  a least upper bound of \(S\).
\end{proof}

\begin{definition}
  Let \(L\) be a join-semilattice.
  A subset \(M \subseteq L\) is said to be a join-subsemilattice of \(L\) if
  \(M\) is too a lattice.
  Similarly a meet-subsemilattice is defined.
  If \(M\) is both a meet- and join-subsemilattice, then \(M\) is called a
  sublattice.
\end{definition}

If there is no confusion to which kind of lattice is considered for example if
only join-semilattices are considered, then a join-subsemilattice will be by
abuse of language simply be called a sublattice.

\begin{example}
  Let \(F\) be a filter in a meet-semilattice (for example in a powerset).
  Then \(F\) is a sublattice, as the following argument demonstrates.
  Suppose \(x,y \in F\).
  Then since \(F\) is downward directed there is \(z \in F\) such that
  \(z \le x\) and \(z \le y\).
  Now \(z \le x \wedge y\), so due to \(F\) being upward closed
  \(x \wedge y \in F\).
  Hence, every two elements has a greatest lower bound in \(F\).

  Furthermore suppose \(F\) is a filter in a lattice.
  Let \(x,y \in F\).
  Then \(x \le x \vee y\).
  Hence, by upward closedness of \(F\), \(x \vee y \in F\).
  Therefore, \(F\) is in fact a sublattice.
\end{example}

\subsection{Totally Ordered Sets}

\begin{definition}
  If \((T,\le)\) is a totally ordered set, then the function
  \[ \sup: T \times T \to T \]
  is defined as
  \[
    \sup(a,b) =
      \begin{cases}
        a & \text{if } b \le a \\
        b & \text{if } a < b.
      \end{cases}
  \]
  \(\sup(a,b)\) is called the supremum of \(a\) and \(b\).
\end{definition}

Similarly one defines the infinimum.

\begin{definition}
  If \((T,\le)\) is totally ordered set, then the function
  \[ \inf: T \times T \to T \]
  is defined as
  \[
    \inf(a,b) =
      \begin{cases}
        b & \text{if } b \le a \\
        a & \text{if } a < b.
      \end{cases}
  \]
  \(\inf(a,b)\) is called the infimum of \(a\) and \(b\).
\end{definition}

It is easy to verify that \(\sup(a,b)\) is the least upper bound of \(\{a,b\}\).
Therefore all lattice theoretic concepts carry over on totally ordered sets.
Moreover every finite subset \(S = \{a_1,\dots,a_n\}\) has a supremum which can
be denoted by
\[ \sup S = \sup_{a \in S} a = \sup_{k=1}^n a_k. \]
A similar result holds for the infimum.
The notions of completeness carry over.

As with lattices one can introduce artificial bounds, in order to make a toset
bounded.
Therefore if \(T\) has no element \(e\) such that
\[ \sup(e,x) = \sup(x,e) = x \]
then it is possible to extend \(T\) to a commutative monoid, \(T^e\), where
the above equality is defined to hold.
This \(e\) is usually denoted with the symbol \(-\infty\).
In effect this extension makes it reasonable to define
\[ -\infty \le x \]
for all \(x \in T^{-\infty}\).
Since \(T^{-\infty}\) is a monoid the supremum of the empty set is defined to
be
\[ \sup \varnothing = -\infty. \]
Similarly \(\inf \varnothing = \infty\) can be motivated.

Note that it can be sensible to use either \(-\infty\), as above, or \(0\), as
in the case of lattices, to denote the lower bound.
It depends on the case at hand.
Same for \(\infty\) and \(1\).

For finite subsets \(S\) of a totally ordered set one introduces the notation
\[ \min S = \inf S \qandq \max S = \sup S, \]
and are read as the minimum of \(S\) and the maximum of \(S\).
More generally if \(S\) is any subset with a maximum, the maximum of \(S\) is
denoted by \(\max S\).
Similarly for \(\min\).
Although \(\min\) and \(\max\) are defined as functions defined on a subset of
the power set, they shall commonly be viewed as functions of \(n\)-variables.
For example one may write
\[ \min(a,b) \]
instead of \(\min\{a,b\}\).
Despite \(\min\) and \(\inf\) being synonyms the former will be used to
emphasize that the set in question actually has a minimum.

The subsection is continued with some hard work related to tosets.

\begin{definition}
  Let \(T\) be a totally ordered set.
  A subset \(I \subseteq T\) is an interval if whenever \(y \in T\) and
  \(x \le y \le z\) for some \(x,z \in I\), then \(y \in I\).
  In symbols
  \[ \forall y \in T: \exists x,z \in I: x \le y \le z \impliess y \in I. \]
\end{definition}

\begin{example}
  In any toset \(T\) both \(\varnothing\) and \(T\) are intervals.
\end{example}

\begin{definition}[Rays]
  The open rays determined by \(a\) are defined by
  \[
    (-\infty,a) = \{x \in T \ssep x < a\}
    \qandq
    (a,\infty) = \{x \in T \ssep x > a\}.
  \]
  The closed rays determined by \(a\) are defined as
  \[
    (-\infty,a] = \{x \in T \ssep x \le a\}
    \qandq{}
    [a,\infty) = \{x \in T \ssep x \ge a\}.
  \]
\end{definition}

\begin{proposition}
  All rays are intervals.
\end{proposition}
\begin{proof}
  Let \(a \in T\) be given.
  The proof is carried out only for \((-\infty,a)\) and \([a,\infty)\), since
  the proof of the other two cases is similar.
  Suppose \(y \in T\) and that \(x \le y \le z\) for some
  \(x,z \in (-\infty,a)\).
  Then \(z < a\) and \(y < a\) follows.
  Hence \(y \in (-\infty,a)\) and it follows that \((-\infty,a)\) is an
  interval.
  Similarly if \(y \in T\) and \(x \le y \le z\) for some
  \(x,z \in{} [a,\infty)\), then \(x \ge a\), so \(y \ge a\) by transitivity.
  Thus, \(y \in{} [a,\infty)\) by transitivity and \([a,\infty)\) satisfies the
  definition of being an interval.
\end{proof}

\begin{definition}
  The following sets are called cells determined by \(a\) and \(b\):
  \begin{gather*}
    (a,b) = \{x \in T \ssep a < x < b\},
    \quad{}
    [a,b] = \{x \in T \ssep a \le x \le b\}, \\
    [a,b) = \{x \in T \ssep a \le x < b\},
    \quad{}
    (a,b] = \{x \in T \ssep a < x \le b\}.
  \end{gather*}
  The set \((a,b)\) is called open and the set \([a,b]\) is called closed.
\end{definition}

\begin{proposition}
  All cells are intervals.
\end{proposition}
\begin{proof}
  The proof is only carried out for \((a,b)\) since the proof of the others are
  very similar.
  Let \(y \in T\) and suppose \(x \le y \le z\) for some \(x,z \in (a,b)\).
  Then the definition gives
  \[ a < x \le y \le z < b \]
  which means \(a < y < b\) and so \(y \in (a,b)\).
\end{proof}

It turns out these sets constitute all intervals on \(T\).

\begin{proposition}
  Let \(T\) be a bounded complete totally ordered set.
  If \(I \subseteq T\) is an interval, then \(I\) equals one of
  \begin{gather*}
    \varnothing, \quad (a,b), \quad{} [a,b), \quad (a,b], \quad{} [a,b], \\
    (-\infty,a), \quad (-\infty,a], \quad (a,\infty), \quad{} [a,\infty),
      \quad T,
  \end{gather*}
  for some \(a,b \in T\).
\end{proposition}

The proof is simple, but messy.
Note that the bounded completeness is crucial.
This can be seen later, when the real numbers have been constructed and some
number theory is in place.
% FIXME: prove that completeness is necessary here

\begin{proposition}\label{prop:tot-interval-laws}
  Let \(T\) be a totally ordered set.
  For all \(a,b,c,d \in T\) one has
  \begin{gather}
    (a,\infty) \cap (c,\infty) = (\max(a,c),\infty),
      \label{eq:tot-intersect-pp-rays} \\
    (-\infty,a) \cap (-\infty,c) = (-\infty,\min(a,c)),
      \label{eq:tot-intersect-mm-rays} \\
    (a,\infty) \cap (-\infty,c) = (a,c),\label{eq:tot-intersect-pm-rays} \\
    (a,b) \cap (c,d) = (\max(a,c),\min(b,d)),
      \label{eq:tot-intersect-bb} \\
    (a,b) \cap (-\infty,c) = (a,\min(b,c)) \\
    (a,b) \cap (c,\infty) = (\max(a,c),b)
  \end{gather}
\end{proposition}
\begin{proof}
  \eqref{eq:tot-intersect-pp-rays} follows from
  \begin{align*}
    x \in (a,\infty) \cap (c,\infty)
      &\iffs x \in (a,\infty) \text{ and } x \in (c,\infty) \\
      &\iffs x > a \text{ and } x > c \\
      &\iffs x > \max(a,c) \\
      &\iffs x \in (\max(a,c),\infty).
  \end{align*}

  \eqref{eq:tot-intersect-mm-rays} follows by duality from
  \eqref{eq:tot-intersect-pp-rays}.

  \eqref{eq:tot-intersect-pm-rays} follows from
  \begin{align*}
    x \in (a,\infty) \cap (-\infty,c)
      &\iffs x \in (a,\infty) \text{ and } x \in (-\infty,c) \\
      &\iffs x > a \text{ and } x < c\\
      &\iffs a < x < c \\
      &\iffs x \in (a,b).
  \end{align*}

  For \eqref{eq:tot-intersect-bb} one has by the previous formulas
  \begin{align*}
    (a,b) \cap (c,d)
      &= (a,\infty) \cap (-\infty,b) \cap (c,\infty) \cap (-\infty,d) \\
      &= (\max(a,c),\infty) \cap (-\infty,\min(b,d)) \\
      &= (\max(a,c),\min(b,d)).
  \end{align*}
  The proofs of the last two are similar.
\end{proof}

\begin{definition}
  A sequence \((S_n)_{n=0}^\infty\) is said to be nested if
  \[ S_{n+1} \subseteq S_n \]
  for all \(n \in \setN\).
\end{definition}

\begin{named}[Nested cells property of totally ordered sets]
  A totally ordered set \(T\) is said to have the nested cells property if
  for all nested sequences of non-empty closed cells \((I_n)_{n=0}^\infty\)
  there is an \(x \in T\) such that \(x \in I_n\) for all \(n \in \setN\).
\end{named}

\begin{lemma}
  A closed cell \(I\) is non-empty iff \(I = [a,b]\) where \(a \le b\).
\end{lemma}
\begin{proof}
  If \(a \le b\), then clearly \(I = [a,b]\) is a non-empty closed cell.
  Suppose \(I\) is a non-empty closed cell.
  Then \(I = [a,b]\) for some \(a,b \in T\).
  If \(b < a\), then there is no \(x \in I\) since that would imply the
  contradiction
  \[ x \le b < a \le x. \]
  This shows
  \[ b < a \impliess I = \varnothing, \]
  which is equivalent to
  \[ I \ne \varnothing \impliess a \le b. \qedhere \]
\end{proof}
\begin{lemma}
  If \(c \le d\), then
  \[ [c,d] \subseteq{} [a,b] \impliess a \le c \le d \le b. \]
\end{lemma}
\begin{proof}
  The assumption \(c \le d\) guarantees \(c \in{} [c,d]\) and \(d \in{} [c,d]\).
  Since \(c \in{} [c,d]\) the assumption gives \(c \in{} [a,b]\) so that
  \(a \le c\).
  Similarly one gets \(d \le b\).
  Now
  \[ a \le c \le d \le b. \qedhere \]
\end{proof}

\begin{theorem}
  All bounded complete totally ordered sets have the nested cells property.
\end{theorem}
\begin{proof}
  Let \(T\) be bounded complete and let \((I_n)_{n=0}^\infty\) be a sequence of
  closed nested cells.
  For all \(n\) let \(I_n = [a_n,b_n]\).
  Then for all \(n\) one has
  \[ [a_{n+1},b_{n+1}] = I_{n+1} \subseteq I_n = [a_n,b_n]. \]
  The two lemmas give
  \[ a_n \le a_{n+1} \le b_{n+1} \le b_n. \]
  Using some induction one can show that every \(b_m\) where \(m \in \setN\) is
  an upper bound of the set
  \[ A = \{a_n \ssep n \in \setN\}. \]
  Clearly \(A\) is non-empty.
  Completeness now gives that \(A\) has a least upper bound, \(\xi\).
  Because \(\xi\) is an upper bound of \(A\) one has \(a_n \le \xi\) for all
  \(n \in \setN\).
  Since \(\xi\) is the least upper bound one has \(\xi \le b_n\) for all
  \(n \in \setN\).
  All in all
  \[ a_n \le \xi \le b_n \]
  for all \(n \in \setN\), but this means \(\xi \in I_n\) for all
  \(n \in \setN\) completing the proof.
\end{proof}
